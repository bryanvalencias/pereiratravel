package controls;

import java.util.ArrayList;
import java.util.List;

import model.Category;
import pereira.travel.R;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.TextView;
import controls.adapterGridView.OnListenerLoadingCategories;
import extras.Syncing;
import extras.Syncing.OnGetListKeyWordsListener;
import extras.Syncing.OnIsStoredListener;

public class FragmentCategoriesMain extends Fragment implements
		OnItemClickListener, OnIsStoredListener, OnListenerLoadingCategories,
		OnGetListKeyWordsListener {

	private GridView gv_list_categories;
	private TextView tvResultMessage;
	private onClickListenerGridView itemGridViewClicked;
	private OnNotifyDataSetChanged notifyListener;
	private OnNotifyStoredKeyWord notifyKeyWordsListener;
	private TextView tvTitle;

	List<Category> categoryList;

	ProgressDialog pd;

	private float level = 0;
	private Syncing sync = null;

	public interface onClickListenerGridView {
		public void onClickListenerItemGridView(int position, String name);
	}

	public interface OnNotifyDataSetChanged {
		public void notifyDataSetChanged();

	}

	public interface OnNotifyStoredKeyWord {
		public void notifyStoredKeyWord(List<String> ListKeyWords);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setHasOptionsMenu(true);
	}

	@Override
	public void onResume() {
		this.notifyListener.notifyDataSetChanged();
		this.gv_list_categories.setOnItemClickListener(this);
		super.onResume();
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		super.onCreateOptionsMenu(menu, inflater);

		if (menu != null) {
			try {
				menu.removeGroup(R.id.groupMenu);

			} catch (Exception e) {
				Log.e("menuException", "ERROOOOOOOOOOOOOOOOOOOR");
			}
		}
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);

		try {
			this.itemGridViewClicked = (onClickListenerGridView) activity;
			this.notifyListener = (OnNotifyDataSetChanged) activity;
			this.notifyKeyWordsListener = (OnNotifyStoredKeyWord) activity;

		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString()
					+ " must implement onButtonClickListener");
		}

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_categories_main,
				container, false);
		this.gv_list_categories = (GridView) view
				.findViewById(R.id.gv_list_categories);
		this.tvResultMessage = (TextView) view
				.findViewById(R.id.tvResultMessage);
		tvTitle = (TextView) view.findViewById(R.id.tvTitleCat);
		tvTitle.setText(R.string.stCategories);
		this.level = this.getResources().getDisplayMetrics().density;
		this.sync = new Syncing(this.getActivity(), this, this);
		pd = ProgressDialog.show(this.getActivity(), null, this.getActivity()
				.getString(R.string.Loading) + "...", true, true);
		if (savedInstanceState == null) {
			if (this.sync.isOnline()) {
				this.sync.DownLoad(true, null);
				this.sync.DownLoadKeywords(false);
			} else {
				this.sync.DownLoad(false, null);
			}
		} else {

			this.sync.DownLoad(false, null);
			Log.e("network", "offline");
		}
		return view;
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int position,
			long arg3) {
		this.itemGridViewClicked.onClickListenerItemGridView(position,
				this.categoryList.get(position).getName());
	}

	@Override
	public void onDataStoredListener(List<Category> categoryListy) {
		this.categoryList = categoryListy;
		Syncing.getAvailableInternalMemorySize();
		if (Syncing.outMemory) {

			AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
			alert.setTitle("Sin memoria");
			alert.setMessage("La memoria es insuficiente para el funcionamiento de Pereira.Travel");
			alert.setNegativeButton("Cerrar",
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							getActivity().finish();

						}
					});
			alert.create();
			alert.show();
			pd.dismiss();
		} else {
			this.gv_list_categories.setAdapter(new adapterGridView(
					getActivity(), (ArrayList<Category>) categoryListy, level,
					this));
		}

	}

	@Override
	public void onMessageError(String text) {
		this.tvResultMessage.setText(text);
		pd.dismiss();

	}

	@Override
	public void onListenerLoadingCategoriesComplete() {
		pd.dismiss();

	}

	@Override
	public void onGetListKeyWordsListener(List<String> listKeyWords) {
		this.notifyKeyWordsListener.notifyStoredKeyWord(listKeyWords);

	}

}
