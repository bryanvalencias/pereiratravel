package controls;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageLoadingListener;
import com.nostra13.universalimageloader.core.assist.SimpleImageLoadingListener;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;

import pereira.travel.R;
import model.Article;
import android.app.Activity;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class adapterEvent extends ArrayAdapter<Article> {
	Activity activity;
	ArrayList<Article> articleList;
	private DisplayImageOptions ImageOptions = null;
	private ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();

	public adapterEvent(Activity activity, ArrayList<Article> articleList) {
		super(activity, R.layout.item_event, articleList);
		this.activity = activity;
		this.articleList = articleList;

	}

	private DisplayImageOptions setDisplayImageOptions() {
		ImageOptions = new DisplayImageOptions.Builder()
				.bitmapConfig(Bitmap.Config.RGB_565)
				.showImageOnLoading(R.drawable.loading)
				.showImageForEmptyUri(R.drawable.loading).cacheInMemory(true)
				.cacheOnDisc(true).considerExifParams(true).build();
		return ImageOptions;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view = convertView;
		VistaItem vistaItem;
		if (view == null) {
			LayoutInflater inflater = this.activity.getLayoutInflater();
			view = inflater.inflate(R.layout.item_event, null);
			vistaItem = new VistaItem();
			vistaItem.title = (TextView) view.findViewById(R.id.text_event);
			vistaItem.date = (TextView) view.findViewById(R.id.date_event);
			vistaItem.image = (ImageView) view.findViewById(R.id.image_event);
			view.setTag(vistaItem);
		} else {
			vistaItem = (VistaItem) view.getTag();
		}

		vistaItem.title.setText(this.articleList.get(position).getTitle());
		vistaItem.date.setText(this.articleList.get(position).getDate());

		ImageLoader.getInstance().displayImage(
				articleList.get(position).getImage(), vistaItem.image,
				setDisplayImageOptions(), animateFirstListener);

		return (view);
	}

	private class AnimateFirstDisplayListener extends
			SimpleImageLoadingListener {

		final List<String> displayedImages = Collections
				.synchronizedList(new LinkedList<String>());

		@Override
		public void onLoadingComplete(String imageUri, View view,
				Bitmap loadedImage) {
			if (loadedImage != null) {
				ImageView imageView = (ImageView) view;
				boolean firstDisplay = !displayedImages.contains(imageUri);
				if (firstDisplay) {
					FadeInBitmapDisplayer.animate(imageView, 500);
					displayedImages.add(imageUri);
				}
			}
		}
	}

	public static class VistaItem {
		TextView title;
		TextView date;
		ImageView image;
	}
}
