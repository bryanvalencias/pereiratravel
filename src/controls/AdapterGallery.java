package controls;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageLoadingListener;
import com.nostra13.universalimageloader.core.assist.SimpleImageLoadingListener;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;

import pereira.travel.R;
import android.app.Activity;
import android.graphics.Bitmap;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

public class AdapterGallery extends BaseAdapter {

	private Activity activity;
	private ArrayList<String> arrayImage;
	DisplayImageOptions ImageOptions = null;
	private ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();

	public AdapterGallery(Activity activity, ArrayList<String> arrayImage) {
		this.activity = activity;
		this.arrayImage = arrayImage;
	}

	private DisplayImageOptions setDisplayImageOptions() {
		ImageOptions = new DisplayImageOptions.Builder()
				.showImageOnLoading(R.drawable.loading)
				.showImageForEmptyUri(R.drawable.loading)
				.showImageOnFail(R.drawable.oops).cacheInMemory(true)
				.cacheOnDisc(true).considerExifParams(true).build();
		return ImageOptions;
	}

	@Override
	public int getCount() {
		return this.arrayImage.size();
	}

	@Override
	public Object getItem(int position) {
		return position;
	}

	@Override
	public long getItemId(int position) {

		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ImageView imageView = new ImageView(activity);
		imageView.setLayoutParams(new GridView.LayoutParams(150, 150));
		imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
		ImageLoader.getInstance().displayImage(arrayImage.get(position),
				imageView, setDisplayImageOptions(), animateFirstListener);
		return imageView;

	}

	private class AnimateFirstDisplayListener extends
			SimpleImageLoadingListener {

		final List<String> displayedImages = Collections
				.synchronizedList(new LinkedList<String>());

		@Override
		public void onLoadingComplete(String imageUri, View view,
				Bitmap loadedImage) {
			if (loadedImage != null) {
				ImageView imageView = (ImageView) view;
				boolean firstDisplay = !displayedImages.contains(imageUri);
				if (firstDisplay) {
					FadeInBitmapDisplayer.animate(imageView, 500);
					displayedImages.add(imageUri);
				}
			}

		}
	}

}
