package controls;

import java.util.ArrayList;
import java.util.List;

import controls.AdapterListSites.OnListenerLoadingSites;
import model.Category;
import model.Site;
import pereira.travel.Main;
import pereira.travel.R;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;

import extras.Syncing;
import extras.Syncing.OnGetListSitesListener;
import extras.Syncing.OnIsStoredListener;

public class FragmentListSites extends Fragment implements OnItemClickListener,
		OnListenerLoadingSites, OnIsStoredListener, OnGetListSitesListener {

	private TextView tvResultMessageListSite;
	private TextView tvResultListSite;
	private ListView lv_listsites;
	private onClickItemSiteListener itemListener;
	private OnNotifyDataSetChangedListSites onNotifyListener;

	Syncing sync;
	Bundle args;
	List<Category> categoryList;
	List<Site> siteList;
	ProgressDialog pd;
	AdapterListSites adapterList;
	Menu mMenu;

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		pd = ProgressDialog.show(this.getActivity(), null, this.getActivity()
				.getString(R.string.Loading) + "...", true, true);
		if (savedInstanceState == null) {
			sync = new Syncing(getActivity(), this, this);
			Bundle args = this.getArguments();
			sync.DownLoad(false, Main.ONE);
			if (args != null) {
				this.args = args;
			}
		} else {
			this.setAdapterList(savedInstanceState);
		}
		this.lv_listsites.setOnItemClickListener(this);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setHasOptionsMenu(true);
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putParcelableArrayList(
				"SiteList",
				(ArrayList<? extends Parcelable>) (ArrayList<Site>) this.siteList);

	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		super.onCreateOptionsMenu(menu, inflater);
		Configuration config = getResources().getConfiguration();
		if (menu != null) {
			try {
				menu.removeGroup(R.id.groupMenu);
				if (config.orientation == Configuration.ORIENTATION_PORTRAIT) {
					menu.setGroupVisible(R.id.groupMenu, true);
					this.setHasOptionsMenu(true);
				}
			} catch (Exception e) {
				Log.e("menuException", "ERROOOOOOOOOOOOOOOOOOOR");
			}
		}
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			this.itemListener = (onClickItemSiteListener) activity;
			this.onNotifyListener = (OnNotifyDataSetChangedListSites) activity;

		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString()
					+ " must implement onButtonClickListener");
		}

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);

		final View view = inflater.inflate(R.layout.fragment_listsite_main,
				container, false);
		this.lv_listsites = (ListView) view.findViewById(R.id.lv_listsites);
		this.tvResultMessageListSite = (TextView) view
				.findViewById(R.id.tvResultMessageListSite);
		this.tvResultListSite = (TextView) view.findViewById(R.id.tvTitleSite);
		return view;
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int pos, long arg3) {
		adapterList.notifyDataSetChanged();
		if (this.itemListener != null) {
			this.itemListener.onClickItemListSiteListener(siteList.get(pos));
		}
	}

	private void initListSiteWithData(Bundle args) {
		String getString = args.getString(Main.QUERY_SEARCH);
		int getInt = args.getInt(Main.POSITION);
		String text = args.getString(Main.TEXT);
		String nameCategory = null;
		Log.e("Bundle", getString);
		Log.e("Bundle", String.valueOf(getInt));
		if (!getString.equals("null")) {
			sync.downLoadSites(null, getString, null);
		}
		if (getInt != -1) {
			for (Category cat : categoryList) {
				if (cat.getIdRoot() != 0) {
					if (cat.getName().equals(text)) {
						getInt = cat.getIdCategory();
						nameCategory = cat.getName();
						break;
					}
				}
			}
			sync.downLoadSites(String.valueOf(getInt), null, null);
		}
		if (nameCategory != null) {
			this.tvResultListSite.setText(nameCategory);
		}
		onNotifyListener.notifyDataSetChangedListSites();

	}

	public void setParameterToSearch(String query) {
		sync.downLoadSites(null, query, null);
	}

	public interface onClickItemSiteListener {
		public void onClickItemListSiteListener(Site site);
	}

	public interface OnNotifyDataSetChangedListSites {
		public void notifyDataSetChangedListSites();

	}

	@Override
	public void onDataStoredListener(List<Category> categoryListy) {
		categoryList = categoryListy;
		this.initListSiteWithData(args);

	}

	@Override
	public void onGetListSiteListener(List<Site> listSite) {

		ArrayList<Site> sitesList = (ArrayList<Site>) listSite;
		adapterList = new AdapterListSites(getActivity(),
				(ArrayList<Site>) sitesList, this);
		this.lv_listsites.setAdapter(adapterList);
		adapterList.notifyDataSetChanged();
		this.siteList = listSite;

	}

	@Override
	public void onMessageError(String text) {
		this.tvResultMessageListSite.setText(text);
		pd.dismiss();

	}

	private void setAdapterList(Bundle bundle) {
		ArrayList<Site> sitesList = bundle.getParcelableArrayList("SiteList");
		if (sitesList != null) {
			adapterList = new AdapterListSites(getActivity(),
					(ArrayList<Site>) sitesList);
			this.lv_listsites.setAdapter(adapterList);
			adapterList.notifyDataSetChanged();
			this.siteList = sitesList;
			pd.dismiss();
		}

	}

	public void reloadListSite(String parameterIdCategory,
			String parameterKeyWord) {
		if (parameterIdCategory != null) {
			sync.downLoadSites(parameterIdCategory, null, null);
		} else if (parameterKeyWord != null) {
			sync.downLoadSites(null, parameterKeyWord, null);
		}
	}

	@Override
	public void onListenerLoadingSitesComplete() {
		pd.dismiss();

	}

}
