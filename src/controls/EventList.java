package controls;

import java.util.ArrayList;
import java.util.List;

import model.Article;
import pereira.travel.Main;
import pereira.travel.R;
import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;
import extras.Syncing;
import extras.Syncing.OnGetListArticleListener;

public class EventList extends Fragment implements OnGetListArticleListener,
		OnItemClickListener {
	Syncing sync;
	private ListView listEvent;
	private adapterEvent adapter;
	ArrayList<Article> listArticle;
	OnClickItemEventListener listener;
	private static String ID_CATEGORY_ARTICLE = "3";
	private static String NUM_ARTICLE = "20";

	public EventList() {

	}

	public interface OnClickItemEventListener {
		public void onClickItemEventListener(Article art);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.fragment_event, container,
				false);
		listEvent = (ListView) rootView.findViewById(R.id.listEvent);
		sync = new Syncing(this.getActivity(), this);
		sync.DownLoadListArticle(sync.isOnline(), Main.URL,
				ID_CATEGORY_ARTICLE, NUM_ARTICLE);
		listEvent.setOnItemClickListener(this);
		this.getActivity().getActionBar().setDisplayHomeAsUpEnabled(true);
		this.getActivity().getActionBar().setHomeButtonEnabled(true);
		setHasOptionsMenu(true);
		this.getActivity().setProgressBarIndeterminateVisibility(true);
		return rootView;
	}

	@Override
	public void onGetListArticleListener(List<Article> listArticle) {
		ArrayList<Article> articleList = (ArrayList<Article>) listArticle;
		adapter = new adapterEvent(getActivity(), articleList);
		listEvent.setAdapter(adapter);
		this.listArticle = articleList;
		this.getActivity().setProgressBarIndeterminateVisibility(false);
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		super.onCreateOptionsMenu(menu, inflater);
		this.getActivity().getMenuInflater().inflate(R.menu.event, menu);

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		switch (id) {
		case android.R.id.home:
			this.getActivity().finish();
			return true;

		default:
			break;
		}

		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			listener = (OnClickItemEventListener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString()
					+ " must implement onButtonClickListener");
		}
	}

	@Override
	public void onGetErrorListArticleListener(String Error) {
		Toast.makeText(getActivity(), Error, Toast.LENGTH_SHORT).show();
		this.getActivity().setProgressBarIndeterminateVisibility(false);

	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		listener.onClickItemEventListener(this.listArticle.get(position));
	}

}
