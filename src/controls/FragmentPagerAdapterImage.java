package controls;

import java.util.ArrayList;
import java.util.List;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

public class FragmentPagerAdapterImage extends FragmentStatePagerAdapter {

	List<ScreenSlidePageFragment> fragments;

	public FragmentPagerAdapterImage(FragmentManager fm,
			ArrayList<ScreenSlidePageFragment> fragmentList) {
		super(fm);
		this.fragments = fragmentList;
	}

	@Override
	public Fragment getItem(int arg0) {
		return this.fragments.get(arg0);
	}

	@Override
	public int getCount() {
		return this.fragments.size();
	}

}
