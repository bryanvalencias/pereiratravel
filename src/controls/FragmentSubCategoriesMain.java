package controls;

import java.util.ArrayList;

import model.Category;
import pereira.travel.Main;
import pereira.travel.R;
import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import extras.Syncing;
import extras.Syncing.OnReturnArraySubCategoriesListener;

public class FragmentSubCategoriesMain extends Fragment implements
		OnItemClickListener, OnReturnArraySubCategoriesListener {

	private ListView lv_subcategories;
	private TextView tvTitle;
	private onClickListenerItemList itemListViewClicked;
	private OnNotifyDataSetChangedSubCategories onNotifyListener;

	private int position = -1;
	Syncing sync;
	AdapterListSubCategories adapter;
	ArrayList<String> listSubcategory;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setHasOptionsMenu(true);

	}

	public interface onClickListenerItemList {
		public void onClickListenerItem(int position, String text);
	}

	public interface OnNotifyDataSetChangedSubCategories {
		public void notifyDataSetChangedSubCategories();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		this.onNotifyListener.notifyDataSetChangedSubCategories();
		return inflater.inflate(R.layout.fragment_subcategoires_main,
				container, false);

	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		this.lv_subcategories = (ListView) this.getView().findViewById(
				R.id.lv_subcategories);
		this.tvTitle = (TextView) this.getView().findViewById(R.id.tvTitleSub);
		Bundle bundle = this.getArguments();
		this.initSubCategories(bundle);
		this.lv_subcategories.setOnItemClickListener(this);

	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		super.onCreateOptionsMenu(menu, inflater);

		if (menu != null) {
			try {
				menu.removeGroup(R.id.groupMenu);

			} catch (Exception e) {
				Log.e("menuException", "ERROOOOOOOOOOOOOOOOOOOR");
			}
		}

	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			this.itemListViewClicked = (onClickListenerItemList) activity;
			this.onNotifyListener = (OnNotifyDataSetChangedSubCategories) activity;

		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString()
					+ " must implement onButtonClickListener");
		}
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View view, int position,
			long arg3) {
		String text = listSubcategory.get(position);
		adapter.notifyDataSetChanged();
		this.itemListViewClicked.onClickListenerItem(position, text);
	}

	@Override
	public void onReturnArraySubCategoriesListener(
			ArrayList<Category> listCategory) {
		listSubcategory = new ArrayList<String>();
		for (Category category : listCategory) {
			listSubcategory.add(category.getName());
		}
		adapter = new AdapterListSubCategories(getActivity(), listSubcategory);
		this.lv_subcategories.setAdapter(adapter);
		this.lv_subcategories.setDividerHeight(1);

	}

	private void initSubCategories(Bundle getPosition) {
		if (getPosition != null) {
			position = getPosition.getInt(Main.POSITION);
			this.tvTitle.setText(getPosition.getString(Main.NAME));
		}
		if (position != -1) {
			sync = new Syncing(getActivity(), position, this);
			sync.initGetSubCategory();
		}
	}

}
