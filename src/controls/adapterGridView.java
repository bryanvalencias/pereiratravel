package controls;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import model.Category;
import pereira.travel.R;
import android.app.Activity;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageLoadingListener;
import com.nostra13.universalimageloader.core.assist.SimpleImageLoadingListener;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;

public class adapterGridView extends BaseAdapter {
	private Activity ctx;
	private ArrayList<Category> arrayCategories = new ArrayList<Category>();
	private ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();
	// private final float LOW_LEVEL = 0.75f;
	// private final float MEDIUM_LEVEL = 1.0f;
	// private final float HIGH_LEVEL = 1.5f;
	// private final float XHIGH_LEVEL = 2.0f;
	// private final float XXHIGH_LEVEL = 3.0f;
	// private float level = 0;
	DisplayImageOptions ImageOptions = null;
	public OnListenerLoadingCategories listener;
	ImageLoader imageLoader;

	public adapterGridView(Activity context,
			ArrayList<Category> arrayCategories, float level,
			OnListenerLoadingCategories listenerLoading) {
		// super(context, R.layout.gallery_categories, arrayCategories);
		super();
		this.ctx = context;
		this.arrayCategories = arrayCategories;
		// this.level = level;
		listener = listenerLoading;
		imageLoader = ImageLoader.getInstance();

	}

	public interface OnListenerLoadingCategories {
		public void onListenerLoadingCategoriesComplete();
	}

	private DisplayImageOptions setDisplayImageOptions() {
		ImageOptions = new DisplayImageOptions.Builder()
				.showImageOnLoading(R.drawable.loading)
				.showImageForEmptyUri(R.drawable.loading)
				.bitmapConfig(Bitmap.Config.RGB_565)
				.showImageOnFail(R.drawable.oops).cacheInMemory(true)
				.cacheOnDisc(true).considerExifParams(true).build();
		return ImageOptions;
	}

	@Override
	public int getCount() {
		if (this.arrayCategories != null) {
			return this.arrayCategories.size();
		} else {
			return 0;
		}

	}

	@Override
	public Category getItem(int position) {

		return this.arrayCategories.get(position);
	}

	@Override
	public long getItemId(int position) {

		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view = convertView;
		VistaItem vistaItem;

		if (view == null) {
			vistaItem = new VistaItem();
			LayoutInflater inflater = this.ctx.getLayoutInflater();
			view = inflater.inflate(R.layout.gallery_categories, parent, false);
			vistaItem.name = (TextView) view.findViewById(R.id.tv_category);
			vistaItem.image = (ImageView) view.findViewById(R.id.ima_category);
			view.setTag(vistaItem);

		} else {
			vistaItem = (VistaItem) view.getTag();

		}

		// if (level == LOW_LEVEL) {
		// Log.e("level", "LOW_LEVEL " + level);
		// image.setLayoutParams(new GridView.LayoutParams(90, 90));
		// } else if (level == MEDIUM_LEVEL) {
		// Log.e("level", "MEDIUM_LEVEL " + level);
		// image.setLayoutParams(new GridView.LayoutParams(120, 120));
		// } else if (level == HIGH_LEVEL) {
		// Log.e("level", "HIGH_LEVEL " + level);
		// image.setLayoutParams(new GridView.LayoutParams(90, 90));
		// } else if (level == XHIGH_LEVEL) {
		// Log.e("level", "HIGH_LEVEL " + level);
		// image.setLayoutParams(new GridView.LayoutParams(150, 150));
		// if (level == XXHIGH_LEVEL) {
		// Log.e("level", "HIGH_LEVEL " + level);
		// view.setLayoutParams(new GridView.LayoutParams(250, 280));
		// } else {
		// // else {
		// // Log.e("level", "nothing the level is" + level);
		// // image.setLayoutParams(new GridView.LayoutParams(90, 90));
		// // }
		// // view.setLayoutParams(new GridView.LayoutParams(140, 170));
		// }
		Log.e("URL", arrayCategories.get(position).getImagesPath());

		imageLoader
				.displayImage(arrayCategories.get(position).getImagesPath(),
						vistaItem.image, setDisplayImageOptions(),
						animateFirstListener);
		vistaItem.name.setText(arrayCategories.get(position).getName());

		return view;
	}

	public static class VistaItem {
		TextView name;
		ImageView image;
	}

	private class AnimateFirstDisplayListener extends
			SimpleImageLoadingListener {

		final List<String> displayedImages = Collections
				.synchronizedList(new LinkedList<String>());

		@Override
		public void onLoadingComplete(String imageUri, View view,
				Bitmap loadedImage) {
			if (loadedImage != null) {
				ImageView imageView = (ImageView) view;
				boolean firstDisplay = !displayedImages.contains(imageUri);
				if (firstDisplay) {
					FadeInBitmapDisplayer.animate(imageView, 500);
					displayedImages.add(imageUri);
				}
			}
			listener.onListenerLoadingCategoriesComplete();
		}
	}

}
