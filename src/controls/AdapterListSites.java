package controls;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import model.Site;
import pereira.travel.R;
import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageLoadingListener;
import com.nostra13.universalimageloader.core.assist.SimpleImageLoadingListener;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;

public class AdapterListSites extends ArrayAdapter<Site> {

	private Activity activity;
	private ArrayList<Site> listSite;
	private DisplayImageOptions ImageOptions = null;
	private OnListenerLoadingSites listenerLoadingSites;
	private ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();

	public AdapterListSites(Activity activity, ArrayList<Site> listSite,
			OnListenerLoadingSites listener) {
		super(activity, R.layout.item_frag_listsite, listSite);
		this.activity = activity;
		this.listSite = listSite;
		this.listenerLoadingSites = listener;

	}

	public AdapterListSites(Activity activity, ArrayList<Site> listSite) {
		super(activity, R.layout.item_frag_listsite, listSite);
		this.activity = activity;
		this.listSite = listSite;
		this.listenerLoadingSites = null;

	}

	public interface OnListenerLoadingSites {
		public void onListenerLoadingSitesComplete();
	}

	private DisplayImageOptions setDisplayImageOptions() {
		ImageOptions = new DisplayImageOptions.Builder()
				.bitmapConfig(Bitmap.Config.RGB_565)
				.showImageOnLoading(R.drawable.loading)
				.showImageForEmptyUri(R.drawable.loading).cacheInMemory(true)
				.cacheOnDisc(true).considerExifParams(true).build();
		return ImageOptions;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view = convertView;
		VistaItem vistaItem;
		if (view == null) {
			LayoutInflater inflater = this.activity.getLayoutInflater();
			view = inflater.inflate(R.layout.item_frag_listsite, null);
			vistaItem = new VistaItem();
			vistaItem.name = (TextView) view.findViewById(R.id.tv_name_site);
			vistaItem.address = (TextView) view
					.findViewById(R.id.tv_address_site);
			vistaItem.ratings = (TextView) view
					.findViewById(R.id.tv_ratings_site);
			vistaItem.image = (ImageView) view.findViewById(R.id.image_site);
			view.setTag(vistaItem);
		} else {
			vistaItem = (VistaItem) view.getTag();
		}

		vistaItem.name.setText(this.listSite.get(position).getName());
		vistaItem.address.setText(this.listSite.get(position).getAddress());
		Configuration config = this.activity.getResources().getConfiguration();
		if (this.listSite.get(position).getFullRatings().length() != 0) {

			if (!this.isTablet(this.activity)) {
				if (config.orientation == Configuration.ORIENTATION_PORTRAIT) {
					vistaItem.ratings.setText(this.activity
							.getString(R.string.ratingsSite)
							+ " "
							+ this.listSite.get(position).getFullRatings());

				} else {
					vistaItem.ratings.setText("");
				}
			} else {
				if (config.orientation == Configuration.ORIENTATION_PORTRAIT) {
					vistaItem.ratings.setText(this.activity
							.getString(R.string.ratingsSite)
							+ " "
							+ this.listSite.get(position).getFullRatings());

				} else {
					vistaItem.ratings.setText("");
				}
			}
		} else {
			if (!this.isTablet(this.activity)) {
				vistaItem.ratings.setText(this.activity
						.getString(R.string.withOutRatingsSite));

			} else {
				if (config.orientation != Configuration.ORIENTATION_PORTRAIT) {
					vistaItem.ratings.setText("");
				} else {
					vistaItem.ratings.setText(this.activity
							.getString(R.string.withOutRatingsSite));
				}
			}
		}

		ImageLoader.getInstance().displayImage(
				listSite.get(position).getLogo(), vistaItem.image,
				setDisplayImageOptions(), animateFirstListener);

		return (view);
	}

	/* check whether device is tablet */
	private boolean isTablet(Context context) {
		int screenlayout = context.getResources().getConfiguration().screenLayout;
		int sizeMask = Configuration.SCREENLAYOUT_SIZE_MASK;
		int sizeLarge = Configuration.SCREENLAYOUT_SIZE_LARGE;
		boolean result = (screenlayout & sizeMask) >= sizeLarge;
		return result;
	}

	private class AnimateFirstDisplayListener extends
			SimpleImageLoadingListener {

		final List<String> displayedImages = Collections
				.synchronizedList(new LinkedList<String>());

		@Override
		public void onLoadingComplete(String imageUri, View view,
				Bitmap loadedImage) {
			if (loadedImage != null) {
				ImageView imageView = (ImageView) view;
				boolean firstDisplay = !displayedImages.contains(imageUri);
				if (firstDisplay) {
					FadeInBitmapDisplayer.animate(imageView, 500);
					displayedImages.add(imageUri);
				}
			}
			if (listenerLoadingSites != null) {
				listenerLoadingSites.onListenerLoadingSitesComplete();

			}
		}
	}

	public static class VistaItem {
		TextView name;
		TextView address;
		TextView ratings;
		ImageView image;
	}

}
