package controls;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageLoadingListener;
import com.nostra13.universalimageloader.core.assist.SimpleImageLoadingListener;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;

import pereira.travel.EventActivity;
import pereira.travel.R;
import android.app.Fragment;
import android.app.FragmentManager;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

public class detailsArticle extends Fragment {

	Bundle bundle;
	private ImageView imageArticle;
	private TextView textArticle;
	private TextView textDate;
	private TextView textTitle;
	private DisplayImageOptions ImageOptions = null;
	private ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_event_details,
				container, false);
		this.imageArticle = (ImageView) rootView
				.findViewById(R.id.image_article_details);
		this.textArticle = (TextView) rootView
				.findViewById(R.id.text_article_details);
		this.textDate = (TextView) rootView
				.findViewById(R.id.date_article_details);
		this.textTitle = (TextView) rootView
				.findViewById(R.id.title_article_details);
		bundle = this.getArguments();
		this.textArticle.setText(bundle.getString(EventActivity.TEXT_ARTICLE));
		this.textDate.setText(bundle.getString(EventActivity.DATE_ARTICLE));
		this.textTitle.setText(bundle.getString(EventActivity.TITLE_ARTICLE));
		this.getActivity().getActionBar().setDisplayHomeAsUpEnabled(true);
		this.getActivity().getActionBar().setHomeButtonEnabled(true);
		setHasOptionsMenu(true);
		ImageLoader.getInstance().displayImage(
				bundle.getString(EventActivity.IMAGE_ARTICLE),
				this.imageArticle, setDisplayImageOptions(),
				animateFirstListener);

		return rootView;
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		super.onCreateOptionsMenu(menu, inflater);
		this.getActivity().getMenuInflater().inflate(R.menu.event, menu);

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		switch (id) {
		case android.R.id.home:
			FragmentManager fm = getFragmentManager();
			if (fm.getBackStackEntryCount() > 0) {
				fm.popBackStack();
				return true;
			}
			break;
		default:
			break;
		}

		return super.onOptionsItemSelected(item);
	}

	private DisplayImageOptions setDisplayImageOptions() {
		ImageOptions = new DisplayImageOptions.Builder()
				.bitmapConfig(Bitmap.Config.RGB_565)
				.showImageOnLoading(R.drawable.loading)
				.showImageForEmptyUri(R.drawable.loading).cacheInMemory(true)
				.cacheOnDisc(true).considerExifParams(true).build();
		return ImageOptions;
	}

	private class AnimateFirstDisplayListener extends
			SimpleImageLoadingListener {

		final List<String> displayedImages = Collections
				.synchronizedList(new LinkedList<String>());

		@Override
		public void onLoadingComplete(String imageUri, View view,
				Bitmap loadedImage) {
			if (loadedImage != null) {
				ImageView imageView = (ImageView) view;
				boolean firstDisplay = !displayedImages.contains(imageUri);
				if (firstDisplay) {
					FadeInBitmapDisplayer.animate(imageView, 500);
					displayedImages.add(imageUri);
				}
			}
		}
	}

}
