package controls;

import java.util.ArrayList;

import pereira.travel.R;
import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class AdapterListSubCategories extends BaseAdapter {

	private Activity activity;
	private ArrayList<String> listSubCategory;

	public AdapterListSubCategories(Activity activity,
			ArrayList<String> listSubCategory) {
		super();
		this.activity = activity;
		this.listSubCategory = listSubCategory;
	}

	@Override
	public int getCount() {
		if (listSubCategory != null) {
			return listSubCategory.size();
		} else {
			return 0;
		}
	}

	@Override
	public Object getItem(int position) {
		return position;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view = convertView;
		VistaItem vistaItem;
		if (view == null) {
			LayoutInflater inflater = this.activity.getLayoutInflater();
			view = inflater.inflate(R.layout.item_simple_listview, null);
			vistaItem = new VistaItem();
			vistaItem.name = (TextView) view
					.findViewById(R.id.item_simple_listview);

			view.setTag(vistaItem);
		} else {
			vistaItem = (VistaItem) view.getTag();
		}
		vistaItem.name.setText(this.listSubCategory.get(position));
		return (view);
	}

	public static class VistaItem {
		TextView name;

	}
}
