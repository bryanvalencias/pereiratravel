package controls;

import java.util.ArrayList;

import pereira.travel.R;
import model.ObjectMenuDrawer;
import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class AdapterMenuDrawer extends ArrayAdapter<ObjectMenuDrawer> {

	private Activity activity;
	private ArrayList<ObjectMenuDrawer> objectList;

	public AdapterMenuDrawer(Activity context,
			ArrayList<ObjectMenuDrawer> objectsList) {
		super(context, R.layout.item_menu_drawer, objectsList);
		activity = context;
		this.objectList = objectsList;

	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View item = convertView;
		VistaItem vistaitem;
		if (item == null) {
			LayoutInflater inflador = this.activity.getLayoutInflater();
			item = inflador.inflate(R.layout.item_menu_drawer, null);
			vistaitem = new VistaItem();

			vistaitem.image = (ImageView) item
					.findViewById(R.id.imageItemDrawer);
			vistaitem.text = (TextView) item.findViewById(R.id.tvItemDrawer);
			item.setTag(vistaitem);
		} else {
			vistaitem = (VistaItem) item.getTag();
		}

		vistaitem.image.setImageResource(this.objectList.get(position)
				.getImagePath());
		vistaitem.text.setText(this.objectList.get(position).getTextObject());

		return item;
	}

	public static class VistaItem {
		ImageView image;
		TextView text;

	}

}
