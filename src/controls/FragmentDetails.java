package controls;

import java.util.ArrayList;
import java.util.List;

import pereira.travel.GaleryActivity;
import pereira.travel.Main;
import pereira.travel.MapActivity;
import pereira.travel.R;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ResolveInfo;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings.Secure;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import extras.Syncing;
import extras.Syncing.OnResponseRakings;

@SuppressLint("DefaultLocale")
public class FragmentDetails extends Fragment implements OnClickListener,
		OnResponseRakings {

	private Bundle bundle;
	private ImageView logo;
	private TextView nameSite;
	private TextView phoneSite;
	private TextView addressSite;
	private TextView urlSite;
	private TextView emailSite;
	private TextView descriptionSite;
	private TextView fullRatings;
	private ArrayList<String> imageListPath;
	private Button ratings;
	private DisplayImageOptions ImageOptions = null;
	private OnShowInfoListener listener;
	boolean is = false;
	private String latSite;
	private String lonSite;
	private int date = -1;
	private int idSite = -1;
	private String android_id;
	Syncing sync;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		if (!activity.equals(Main.class)) {
			this.listener = (OnShowInfoListener) activity;
		}
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		this.setHasOptionsMenu(true);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.details_site, container, false);
		this.nameSite = (TextView) view.findViewById(R.id.tvNameSite);
		this.fullRatings = (TextView) view
				.findViewById(R.id.tv_ratings_details_site);

		this.phoneSite = (TextView) view.findViewById(R.id.tvPhoneSite);
		this.addressSite = (TextView) view.findViewById(R.id.tvAddressSite);
		this.urlSite = (TextView) view.findViewById(R.id.tvUrlSite);
		this.emailSite = (TextView) view.findViewById(R.id.tvEmailSite);
		this.logo = (ImageView) view.findViewById(R.id.imaSite);
		this.descriptionSite = (TextView) view
				.findViewById(R.id.tvDescriptionSite);
		this.ratings = (Button) view.findViewById(R.id.rating);
		android_id = Secure.getString(getActivity().getContentResolver(),
				Secure.ANDROID_ID);
		this.ratings.setVisibility(View.VISIBLE);
		this.ratings.setOnClickListener(this);

		this.bundle = this.getArguments();
		return view;
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		super.onCreateOptionsMenu(menu, inflater);
		// Locate MenuItem with ShareActionProvider

		if (this.bundle != null) {
			this.putInfoToView();
		}
		if (this.listener != null) {
			listener.onShowInfoListener();
		}
		Configuration config = getResources().getConfiguration();
		if (isTablet(getActivity())) {
			if (config.orientation == Configuration.ORIENTATION_LANDSCAPE)
				inflater.inflate(R.menu.fragment_details, menu);
			if (is) {
				inflater.inflate(R.menu.fragment_details, menu);
			}

		} else {
			inflater.inflate(R.menu.fragment_details, menu);
		}

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		super.onOptionsItemSelected(item);
		Intent intent = new Intent();
		switch (item.getItemId()) {
		case R.id.action_settings2_fragment:
			Bundle bun = new Bundle();
			if (imageListPath.size() > 0) {
				bun.putStringArrayList(GaleryActivity.GALERY, imageListPath);

				intent.setClass(this.getActivity(), GaleryActivity.class);
				intent.putExtra(GaleryActivity.GALERY, bun);
				this.getActivity().startActivity(intent);
			} else {
				Toast.makeText(this.getActivity(),
						this.getActivity().getString(R.string.NullImages),
						Toast.LENGTH_SHORT).show();
			}
			break;
		case R.id.action_map_fragment:
			if (this.latSite.length() > 0 && this.lonSite.length() > 0) {
				intent.setClass(getActivity(), MapActivity.class);
				intent.putExtra(Main.SITEMAP, true);
				intent.putExtra(Main.LAT, this.latSite);
				intent.putExtra(Main.LON, this.lonSite);
				getActivity().startActivity(intent);
			} else {
				Toast.makeText(this.getActivity(),
						this.getActivity().getString(R.string.NullPosition),
						Toast.LENGTH_SHORT).show();
			}
			break;
		case R.id.action_settings_fragment:
			Intent sendIntent = new Intent();
			sendIntent.setAction(Intent.ACTION_SEND);
			sendIntent.putExtra(Intent.EXTRA_TEXT,
					getResources().getText(R.string.site_share1) + " "
							+ this.nameSite.getText().toString() + " "
							+ getResources().getText(R.string.site_share2)
							+ "http://pereira.travel/");
			sendIntent.setType("text/plain");

			// See if official Facebook app is found
			boolean facebookAppFound = false;
			List<ResolveInfo> matches = this.getActivity().getPackageManager()
					.queryIntentActivities(intent, 0);
			for (ResolveInfo info : matches) {
				if (info.activityInfo.packageName.startsWith("com.facebook")) {
					intent.setPackage(info.activityInfo.packageName);
					facebookAppFound = true;
					break;
				}
			}

			// As fallback, launch sharer.php in a browser
			if (!facebookAppFound) {
				String urlToShare = this.urlSite.getText().toString();
				String sharerUrl = "https://www.facebook.com/sharer/sharer.php?u="
						+ urlToShare;
				intent = new Intent(Intent.ACTION_VIEW, Uri.parse(sharerUrl));

			}
			startActivity(Intent.createChooser(sendIntent, getResources()
					.getText(R.string.share)));

		}

		return false;

	}

	private void putInfoToView() {
		ImageLoader.getInstance().displayImage(
				this.bundle.getString(Main.LOGO), this.logo,
				setDisplayImageOptions());
		this.idSite = this.bundle.getInt(Main.SITEID);
		if (this.bundle.getString(Main.FULL_RATINGS).length() != 0) {

			this.fullRatings.setText(this.getActivity().getString(
					R.string.ratingsSite)
					+ " " + this.bundle.getString(Main.FULL_RATINGS));
		} else {
			this.fullRatings.setText(this.getActivity().getString(
					R.string.withOutRatingsSite));
		}
		this.nameSite.setText(this.bundle.getString(Main.NAME));
		this.phoneSite.setText(this.bundle.getString(Main.PHONENUMBRE));
		this.addressSite.setText(this.bundle.getString(Main.ADDRESS));
		this.urlSite.setText(this.bundle.getString(Main.URL));
		this.emailSite.setText(this.bundle.getString(Main.EMAIL));
		this.imageListPath = this.bundle.getStringArrayList(Main.IMAGES);
		this.descriptionSite.setText(bundle.getString(Main.DESCRIPTION));
		this.latSite = bundle.getString(Main.LAT);
		this.lonSite = bundle.getString(Main.LON);
	}

	public void putInfoToView(Bundle bundle) {
		if (bundle != null) {
			is = bundle.getBoolean(Main.SITEMAP);
			ImageLoader.getInstance().displayImage(bundle.getString(Main.LOGO),
					this.logo, setDisplayImageOptions());
			this.idSite = bundle.getInt(Main.SITEID);
			this.nameSite.setText(bundle.getString(Main.NAME));
			if (bundle.getString(Main.FULL_RATINGS).length() != 0) {

				this.fullRatings.setText(this.getActivity().getString(
						R.string.ratingsSite)
						+ " " + bundle.getString(Main.FULL_RATINGS));
			} else {
				this.fullRatings.setText(this.getActivity().getString(
						R.string.withOutRatingsSite));
			}
			this.phoneSite.setText(bundle.getString(Main.PHONENUMBRE));
			this.addressSite.setText(bundle.getString(Main.ADDRESS));
			this.urlSite.setText(bundle.getString(Main.URL));
			this.emailSite.setText(bundle.getString(Main.EMAIL));
			this.imageListPath = bundle.getStringArrayList(Main.IMAGES);
			this.descriptionSite.setText(bundle.getString(Main.DESCRIPTION));
			this.latSite = bundle.getString(Main.LAT);
			this.lonSite = bundle.getString(Main.LON);
		}
	}

	/* check whether device is tablet */
	private boolean isTablet(Context context) {
		int screenlayout = context.getResources().getConfiguration().screenLayout;
		int sizeMask = Configuration.SCREENLAYOUT_SIZE_MASK;
		int sizeLarge = Configuration.SCREENLAYOUT_SIZE_LARGE;
		boolean result = (screenlayout & sizeMask) >= sizeLarge;
		return result;
	}

	private DisplayImageOptions setDisplayImageOptions() {
		ImageOptions = new DisplayImageOptions.Builder()
				.bitmapConfig(Bitmap.Config.RGB_565)
				.showImageOnLoading(R.drawable.loading)
				.showImageForEmptyUri(R.drawable.loading)
				.showImageOnFail(R.drawable.oops).cacheInMemory(true)
				.cacheOnDisc(true).considerExifParams(true).build();
		return ImageOptions;
	}

	public interface OnShowInfoListener {
		public void onShowInfoListener();
	}

	@Override
	public void onClick(View v) {
		AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
		sync = new Syncing(getActivity(), this);
		LayoutInflater inflater = this.getActivity().getLayoutInflater();
		final View Inflator = inflater.inflate(R.layout.rating, null);
		final TextView value = (TextView) Inflator
				.findViewById(R.id.ratingbar_value);
		final RatingBar ratingbar = (RatingBar) Inflator
				.findViewById(R.id.ratingBar);
		alert.setTitle(R.string.ratings);
		alert.setView(Inflator);
		ratingbar
				.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {

					@Override
					public void onRatingChanged(RatingBar ratingBar,
							float rating, boolean fromUser) {
						switch ((int) rating) {
						case 1:
							value.setText(getActivity().getString(
									R.string.muy_mal));
							date = 1;
							break;
						case 2:
							value.setText(getActivity().getString(R.string.mal));
							date = 2;
							break;
						case 3:
							value.setText(getActivity().getString(
									R.string.bueno));
							date = 3;
							break;
						case 4:
							value.setText(getActivity().getString(
									R.string.muy_bueno));
							date = 4;
							break;
						case 5:
							value.setText(getActivity().getString(
									R.string.excelente));
							date = 5;
							break;

						default:
							break;
						}

					}
				});

		alert.setPositiveButton(this.getActivity().getString(R.string.ratings),
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						if (date != -1 && idSite != -1) {
							getActivity().getActionBar()
									.setDisplayHomeAsUpEnabled(false);
							getActivity().getActionBar().setHomeButtonEnabled(
									false);
							sync.getResponseRankings(String.valueOf(idSite),
									String.valueOf(date), android_id);

						} else {
							dialog.cancel();
						}
					}
				});
		alert.setNegativeButton(this.getActivity().getString(R.string.Cancel),
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.cancel();

					}
				});

		alert.create();
		alert.show();
	}

	@Override
	public void onGetResponseRakings(String[] result) {
		this.getActivity().getActionBar().setDisplayHomeAsUpEnabled(true);
		this.getActivity().getActionBar().setHomeButtonEnabled(true);
		Toast.makeText(getActivity(), result[1], Toast.LENGTH_LONG).show();
		this.fullRatings.setText(this.getActivity().getString(
				R.string.ratingsSite)
				+ " " + result[2]);
		SharedPreferences prefs = this.getActivity().getSharedPreferences(
				"calificacion", Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = prefs.edit();
		editor.putBoolean("calificar", true);
		editor.apply();
		Log.e("shared_saved",
				String.valueOf(prefs.getBoolean("calificar", false)));
		if (editor.commit()) {
			Log.e("Shared", ".....apply.......");
		} else {
			Log.e("Shared", ".....no apply.......");

		}
	}

	@Override
	public void onGetResponseErrorRakings(String result) {
		Toast.makeText(getActivity(), result, Toast.LENGTH_LONG).show();
	}

	public interface OnRatingsChange {
		public void onRatingsChange();
	}
}
