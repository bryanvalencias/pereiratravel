package pereira.travel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import model.Category;
import model.Site;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageLoadingListener;
import com.nostra13.universalimageloader.core.assist.SimpleImageLoadingListener;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import extras.GPSHelper;
import extras.GPSHelper.getLocationListener;
import extras.Syncing;
import extras.Syncing.OnGetListSitesListener;
import extras.Syncing.OnGetListSitesListenerNear;
import extras.Syncing.OnIsStoredListener;

@SuppressLint("InflateParams")
public class MapActivity extends FragmentActivity implements
		getLocationListener, OnIsStoredListener, OnInfoWindowClickListener,
		OnGetListSitesListenerNear, OnGetListSitesListener {

	private GoogleMap Gmap;
	static String COLOR_RADIUS = "#22E97900";
	private MarkerOptions markerOptions;
	private Marker lastMarket;
	private GPSHelper gpsHelper = null;
	private CircleOptions circleOptions = null;
	private double lati;
	private double longi;
	private String filterCategory = null;

	Syncing sync;
	HashMap<String, String> MarketList;
	DisplayImageOptions ImageOptions = null;
	private ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();
	private Menu mMenu;
	private List<Category> listCategory = new ArrayList<Category>();
	private static float zoom = 15F;
	private int textMessage = -1;
	Bundle bundle = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
		setContentView(R.layout.activity_map);
		android.app.ActionBar bar = getActionBar();
		bar.setBackgroundDrawable(getResources().getDrawable(
				android.R.color.black));
		Gmap = ((SupportMapFragment) getSupportFragmentManager()
				.findFragmentById(R.id.map_fragment)).getMap();
		getActionBar().setDisplayHomeAsUpEnabled(true);
		setProgressBarIndeterminateVisibility(true);
		getActionBar().setHomeButtonEnabled(true);
		this.setTitle(null);
		Gmap.setOnInfoWindowClickListener(this);

	}

	@Override
	protected void onStart() {
		super.onStart();

		bundle = this.getIntent().getExtras();
		Gmap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
		Gmap.setMyLocationEnabled(true);
		if (bundle != null) {
			double lat = Double.parseDouble(bundle.getString(Main.LAT));
			double lng = Double.parseDouble(bundle.getString(Main.LON));
			this.showTag(lat, lng, true);

		} else {
			this.sync = new Syncing(this, this, this, this);
			LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
			boolean statusOfGPS = manager
					.isProviderEnabled(LocationManager.GPS_PROVIDER);
			if (statusOfGPS) {
				this.startLocations();

			} else {
				this.configGPS();
			}
		}

	}

	@Override
	protected void onStop() {
		super.onStop();
		if (this.lastMarket != null) {
			this.lastMarket.remove();

		}

	}

	@Override
	protected void onDestroy() {
		super.onStop();
		if (this.lastMarket != null) {
			this.lastMarket.remove();

		}
		if (this.gpsHelper != null) {
			this.gpsHelper.Disconnect();

		}

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		if (bundle == null) {
			MenuInflater inflater = getMenuInflater();
			inflater.inflate(R.menu.map, menu);
			mMenu = menu;
			this.sync.DownLoad(false, null);
		}
		return true;
	}

	@Override
	public boolean onKeyUp(int keyCode, KeyEvent event) {

		if (keyCode == KeyEvent.KEYCODE_BACK) {
			this.finish();
			return true;
		}
		return super.onKeyUp(keyCode, event);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem menuItem) {
		switch (menuItem.getItemId()) {
		case android.R.id.home:
			if (bundle == null) {
				Intent homeIntent = new Intent(this, Main.class);
				homeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(homeIntent);
			} else {
				this.finish();
			}

			return true;
		case R.id.empty:
			Toast.makeText(this, this.getString(R.string.filter) + "...",
					Toast.LENGTH_SHORT).show();
			this.filterCategory = null;
			sync.DownLoadListSitesNear(String.valueOf(lati),
					String.valueOf(longi), Syncing.RADIO, this.filterCategory);
			return true;
		}
		if (this.listCategory != null) {
			Toast.makeText(this, this.getString(R.string.filter) + "...",
					Toast.LENGTH_SHORT).show();
			for (Category cat : this.listCategory) {
				if (menuItem.getItemId() == cat.getIdCategory()) {
					this.filterCategory = String.valueOf(cat.getIdCategory());
					Gmap.clear();
					sync.DownLoadListSitesNear(String.valueOf(lati),
							String.valueOf(longi), Syncing.RADIO,
							String.valueOf(cat.getIdCategory()));
					circleOptions = new CircleOptions();
					circleOptions.center(new LatLng(lati, longi));
					circleOptions.radius(Integer.parseInt(Syncing.RADIO));
					circleOptions.fillColor(Color.parseColor(COLOR_RADIUS));
					circleOptions.strokeColor(Color.parseColor(COLOR_RADIUS));
					circleOptions.visible(true);
					Gmap.addCircle(circleOptions);
					break;
				}
			}
		}
		return (super.onOptionsItemSelected(menuItem));
	}

	private void startLocations() {
		gpsHelper = new GPSHelper(this, this);
		gpsHelper.connect();
	}

	private void showTag(double lat, double lng, String text) {
		LatLng latlng = new LatLng(lat, lng);
		markerOptions = new MarkerOptions();
		markerOptions.position(latlng);
		if (text != null)
			markerOptions.title(text);
		markerOptions.icon(BitmapDescriptorFactory
				.fromResource(R.drawable.map_marker));
		this.lastMarket = Gmap.addMarker(markerOptions);

	}

	private void showTag(double lat, double lng, boolean Zoom) {
		CameraUpdate camUpd2;
		LatLng latlng = new LatLng(lat, lng);
		markerOptions = new MarkerOptions();
		markerOptions.position(latlng);
		markerOptions.icon(BitmapDescriptorFactory
				.fromResource(R.drawable.map_marker));
		if (Zoom) {
			camUpd2 = CameraUpdateFactory.newLatLngZoom(latlng, zoom);
			Gmap.animateCamera(camUpd2);
		}
		this.lastMarket = Gmap.addMarker(markerOptions);
		setProgressBarIndeterminateVisibility(false);
	}

	@Override
	public void onConnected(Location l) {
		this.lati = l.getLatitude();
		this.longi = l.getLongitude();
		sync.DownLoadListSitesNear(String.valueOf(l.getLatitude()),
				String.valueOf(l.getLongitude()), Syncing.RADIO,
				this.filterCategory);
		CameraUpdate camUpd2;
		camUpd2 = CameraUpdateFactory.newLatLngZoom(new LatLng(l.getLatitude(),
				l.getLongitude()), zoom);
		Gmap.animateCamera(camUpd2);
	}

	@Override
	public void onDisconnected() {
		System.out.println("Disconnected");

	}

	@Override
	public void onDataStoredListener(List<Category> categoryList) {
		for (Category category : categoryList) {
			mMenu.add(Menu.NONE, category.getIdCategory(), Menu.NONE,
					category.getName());

		}

		listCategory = categoryList;
	}

	@Override
	public void onMessageError(String text) {
		if (isNumeric(text)) {
			for (Category Cat : listCategory) {
				if (Cat.getIdCategory() == textMessage) {
					Toast.makeText(
							this,
							this.getString(R.string.msjFilter1) + " "
									+ Cat.getName() + " "
									+ this.getString(R.string.msjFilter2),
							Toast.LENGTH_LONG).show();
					break;
				}
			}
		} else {
			Toast.makeText(this, text, Toast.LENGTH_LONG).show();
		}

		setProgressBarIndeterminateVisibility(false);

	}

	private boolean isNumeric(String str) {
		try {
			textMessage = Integer.parseInt(str);
		} catch (NumberFormatException nfe) {
			return false;
		}
		return true;
	}

	@Override
	public void onLocationChange(Location location) {
		Gmap.clear();
		sync.DownLoadListSitesNear(String.valueOf(location.getLatitude()),
				String.valueOf(location.getLongitude()), Syncing.RADIO,
				this.filterCategory);

		CameraUpdate camUpd2;
		camUpd2 = CameraUpdateFactory.newLatLngZoom(
				new LatLng(location.getLatitude(), location.getLongitude()),
				zoom);

		Gmap.animateCamera(camUpd2);
		// Add a Radius
		circleOptions = new CircleOptions();
		circleOptions.center(new LatLng(location.getLatitude(), location
				.getLongitude()));

		circleOptions.radius(Integer.parseInt(Syncing.RADIO));
		circleOptions.fillColor(Color.parseColor(COLOR_RADIUS));
		circleOptions.strokeColor(Color.parseColor(COLOR_RADIUS));
		circleOptions.visible(true);
		Gmap.addCircle(circleOptions);
		setProgressBarIndeterminateVisibility(true);

		Log.e("LocationChange", "Location is changed");
	}

	@Override
	public void onInfoWindowClick(Marker marker) {
		if (marker.getTitle() != null) {
			String idSite = MarketList.get(marker.getTitle());
			sync.downLoadSites(null, null, idSite);

		}

	}

	private void showDetail(String logoSitePath, String nameSite,
			String phoneSite, String addressSite, String urlSite,
			String emailSite, String descriptionSite) {

		AlertDialog.Builder alert = new AlertDialog.Builder(this);
		LayoutInflater inflater = this.getLayoutInflater();
		final View inflator = inflater.inflate(R.layout.details_site, null);
		final ImageView logo = (ImageView) inflator.findViewById(R.id.imaSite);
		final TextView name = (TextView) inflator.findViewById(R.id.tvNameSite);
		final TextView phone = (TextView) inflator
				.findViewById(R.id.tvPhoneSite);
		final TextView address = (TextView) inflator
				.findViewById(R.id.tvAddressSite);
		final TextView url = (TextView) inflator.findViewById(R.id.tvUrlSite);
		final TextView email = (TextView) inflator
				.findViewById(R.id.tvEmailSite);
		final TextView description = (TextView) inflator
				.findViewById(R.id.tvDescriptionSite);
		alert.setView(inflator);
		ImageLoader.getInstance().displayImage(logoSitePath, logo,
				setDisplayImageOptions(), animateFirstListener);

		name.setText(nameSite);
		phone.setText(phoneSite);
		address.setText(addressSite);
		url.setText(urlSite);
		email.setText(emailSite);
		description.setText(descriptionSite);
		alert.setNegativeButton(this.getString(R.string.close),
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.cancel();

					}
				});
		alert.create();
		alert.show();

	}

	@Override
	public void onGetListSiteListenerNear(List<Site> listSite) {
		MarketList = new HashMap<String, String>();
		for (Site site : listSite) {
			this.showTag(Double.parseDouble(site.getLatitude()),
					Double.parseDouble(site.getLongitude()), site.getName());
			MarketList.put(lastMarket.getTitle(), String.valueOf(site.getId()));
		}
		setProgressBarIndeterminateVisibility(false);

	}

	@Override
	public void onGetListSiteListener(List<Site> listSite) {
		for (Site site : listSite) {
			showDetail(site.getLogo(), site.getName(), site.getPhoneNumbre(),
					site.getAddress(), site.getURL(), site.getEmail(),
					site.getDescription());
			if (listSite.size() == 1) {
				break;
			}
		}

	}

	private void configGPS() {
		final Intent intent = new Intent(
				Settings.ACTION_LOCATION_SOURCE_SETTINGS);
		intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
		AlertDialog.Builder alert = new AlertDialog.Builder(this);
		alert.setTitle(this.getString(R.string.SettingsGPSTitle));
		alert.setMessage(this.getString(R.string.SettingsGPSMessage));
		alert.setPositiveButton(this.getString(R.string.Settings),
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						startActivity(intent);
					}
				});
		alert.setNegativeButton(this.getString(R.string.Cancel),
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						finish();
					}
				});
		alert.create();
		alert.show();
	}

	private DisplayImageOptions setDisplayImageOptions() {
		ImageOptions = new DisplayImageOptions.Builder()
				.showImageOnLoading(R.drawable.loading)
				.showImageForEmptyUri(R.drawable.loading)
				.showImageOnFail(R.drawable.oops).cacheInMemory(true)
				.cacheOnDisc(true).considerExifParams(true).build();
		return ImageOptions;
	}

	private class AnimateFirstDisplayListener extends
			SimpleImageLoadingListener {

		final List<String> displayedImages = Collections
				.synchronizedList(new LinkedList<String>());

		@Override
		public void onLoadingComplete(String imageUri, View view,
				Bitmap loadedImage) {
			if (loadedImage != null) {
				ImageView imageView = (ImageView) view;
				boolean firstDisplay = !displayedImages.contains(imageUri);
				if (firstDisplay) {
					FadeInBitmapDisplayer.animate(imageView, 500);
					displayedImages.add(imageUri);
				}
			}

		}
	}

}
