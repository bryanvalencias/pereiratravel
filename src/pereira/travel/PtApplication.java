package pereira.travel;

import java.io.File;

import android.app.Application;
import com.nostra13.universalimageloader.cache.disc.impl.UnlimitedDiscCache;
import com.nostra13.universalimageloader.cache.disc.naming.HashCodeFileNameGenerator;
import com.nostra13.universalimageloader.cache.memory.impl.LruMemoryCache;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.download.BaseImageDownloader;
import com.nostra13.universalimageloader.utils.StorageUtils;

public class PtApplication extends Application {
	@Override
	public void onCreate() {
		super.onCreate();

		// Create global configuration and initialize ImageLoader with this
		// configuration
		File cacheDir = StorageUtils.getCacheDirectory(this);
		ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(
				this)
				// default
				.threadPriority(Thread.NORM_PRIORITY - 2)
				.memoryCache(new LruMemoryCache(30 * 1024 * 1024))
				.memoryCacheSize(50 * 1024 * 1024)
				// default
				.discCache(new UnlimitedDiscCache(cacheDir))
				// default
				.discCacheSize(50 * 1024 * 1024)

				.discCacheFileNameGenerator(new HashCodeFileNameGenerator()) // default
				.imageDownloader(new BaseImageDownloader(this)) // default

				.writeDebugLogs().build();
		ImageLoader.getInstance().init(config);
		// Syncing sync = new Syncing(getApplicationContext());
		// if (sync.isOnline()) {
		// sync.DownLoadCategory(true);
		// Toast.makeText(getApplicationContext(),
		// this.getString(R.string.Connected), Toast.LENGTH_LONG)
		// .show();
		// }

	}

}
