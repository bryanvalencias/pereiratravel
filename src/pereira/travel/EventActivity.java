package pereira.travel;

import model.Article;
import android.app.ActionBar;
import android.app.Activity;
import android.os.Bundle;
import android.view.Window;
import controls.EventList;
import controls.EventList.OnClickItemEventListener;
import controls.detailsArticle;

public class EventActivity extends Activity implements OnClickItemEventListener {
	public static String IMAGE_ARTICLE = "IMAGE_ARTICLE";
	public static String TEXT_ARTICLE = "TEXT_ARTICLE";
	public static String TITLE_ARTICLE = "TITLE_ARTICLE";
	public static String DATE_ARTICLE = "DATE_ARTICLE";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
		setContentView(R.layout.activity_event);

		if (savedInstanceState == null) {
			getFragmentManager().beginTransaction()
					.add(R.id.container, new EventList()).commit();
		}
		this.setTitle(null);
		ActionBar bar = getActionBar();
		bar.setBackgroundDrawable(getResources().getDrawable(
				android.R.color.black));

	}

	@Override
	public void onClickItemEventListener(Article art) {
		Bundle bundle = new Bundle();
		bundle.putString(DATE_ARTICLE, art.getDate());
		bundle.putString(TITLE_ARTICLE, art.getTitle());
		bundle.putString(TEXT_ARTICLE, art.getText());
		bundle.putString(IMAGE_ARTICLE, art.getImage());
		detailsArticle frag = new detailsArticle();
		frag.setArguments(bundle);

		getFragmentManager().beginTransaction().addToBackStack(TEXT_ARTICLE)
				.replace(R.id.container, frag).commit();

	}

}
