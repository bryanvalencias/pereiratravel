package pereira.travel;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.MenuItem;
import controls.FragmentDetails;
import controls.FragmentDetails.OnShowInfoListener;

public class DetailsActivity extends ActionBarActivity implements
		OnShowInfoListener {
	FragmentDetails detalle;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_datils);
		getActionBar().setDisplayHomeAsUpEnabled(true);
		getActionBar().setHomeButtonEnabled(true);
		ActionBar bar = getSupportActionBar();
		this.setTitle(null);
		bar.setBackgroundDrawable(getResources().getDrawable(
				android.R.color.black));
		detalle = (FragmentDetails) this.getSupportFragmentManager()
				.findFragmentById(R.id.fragmentDetails);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem menuItem) {
		switch (menuItem.getItemId()) {
		case android.R.id.home:
			Intent homeIntent = new Intent(this, Main.class);
			homeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			this.startActivity(homeIntent);

		}
		return (super.onOptionsItemSelected(menuItem));
	}

	@Override
	public void onShowInfoListener() {
		detalle.putInfoToView(this.getIntent().getExtras());

	}
	


}
