package pereira.travel;

import android.app.ActionBar;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;

public class CallActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_call);
		getActionBar().setDisplayHomeAsUpEnabled(true);
		getActionBar().setHomeButtonEnabled(true);
		ActionBar bar = getActionBar();
		this.setTitle(null);
		bar.setBackgroundDrawable(getResources().getDrawable(
				android.R.color.black));

	}

	public void onClickEvent(View button) {
		switch (button.getId()) {
		case R.id.btn_firefighters_call:
			this.call("119");
			break;
		case R.id.btn_police_call:
			this.call("123");
			break;
		case R.id.btn_ambulance_call:
			this.call("132");
			break;

		default:
			break;
		}
	}

	private void call(String number) {
		try {
			Intent callIntent = new Intent(Intent.ACTION_CALL);
			callIntent.setData(Uri.parse("tel:" + number));
			startActivity(callIntent);
		} catch (ActivityNotFoundException activityException) {
			Log.e("helloandroid dialing example", "Call failed",
					activityException);
		}
	}

	@Override
	public boolean onKeyUp(int keyCode, KeyEvent event) {

		if (keyCode == KeyEvent.KEYCODE_BACK) {
			this.finish();
			return true;
		}

		return super.onKeyUp(keyCode, event);
	}

}
