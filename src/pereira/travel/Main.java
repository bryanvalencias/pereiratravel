package pereira.travel;

import java.util.ArrayList;
import java.util.List;

import model.ObjectMenuDrawer;
import model.Site;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.provider.SearchRecentSuggestions;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.SearchView;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import controls.AdapterMenuDrawer;
import controls.FragmentCategoriesMain;
import controls.FragmentCategoriesMain.OnNotifyDataSetChanged;
import controls.FragmentCategoriesMain.OnNotifyStoredKeyWord;
import controls.FragmentCategoriesMain.onClickListenerGridView;
import controls.FragmentDetails;
import controls.FragmentDetails.OnShowInfoListener;
import controls.FragmentListSites;
import controls.FragmentListSites.OnNotifyDataSetChangedListSites;
import controls.FragmentListSites.onClickItemSiteListener;
import controls.FragmentSubCategoriesMain;
import controls.FragmentSubCategoriesMain.OnNotifyDataSetChangedSubCategories;
import controls.FragmentSubCategoriesMain.onClickListenerItemList;
import extras.MySuggestionProvider;
import extras.Syncing;

public class Main extends ActionBarActivity implements OnItemClickListener,
		onClickListenerGridView, onClickListenerItemList,
		onClickItemSiteListener, OnNotifyDataSetChanged,
		OnNotifyDataSetChangedSubCategories, OnNotifyDataSetChangedListSites,
		OnNotifyStoredKeyWord, OnShowInfoListener {

	private boolean isCurrent = false;
	private DrawerLayout drawerLayout;
	private ListView drawerList;

	/** STATIC **/
	private static String ID_FIRST_FRAGMENT = "ID_FIRST_FRAGMENT";
	private static String ID_SECOND_FRAGMENT = "ID_SECOND_FRAGMENT";
	private static String ID_THIRD_FRAGMENT = "ID_THIRD_FRAGMENT";
	public static String QUERY_SEARCH = "query_result";
	public static String POSITION = "position";
	public static String TEXT = "text";
	public static String ONLINE = "online";
	public static String OFFLINE = "offline";
	public static String ONE = "1";
	public static String LOGO = "logo";

	private ActionBarDrawerToggle drawerToggle;
	private SearchView searchView;

	int now = 0;
	public static String FULL_RATINGS = "full_ratings";
	public static String NAME = "name";
	public static String ADDRESS = "address";
	public static String PHONENUMBRE = "phoneNumbre";
	public static String URL = "url";
	public static String EMAIL = "email";
	public static String IMAGES = "images";
	public static String SITEMAP = "siteMap";
	public static String SITEID = "siteid";
	public static String LAT = "lat";
	public static String LON = "lon";
	public static String DESCRIPTION = "description";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_main);
		drawerList = (ListView) findViewById(R.id.left_drawer);
		drawerLayout = (DrawerLayout) findViewById(R.id.main_drawer_layout);
		drawerList.setOnItemClickListener(this);
		ActionBar bar = getSupportActionBar();
		this.setTitle(null);

		bar.setBackgroundDrawable(getResources().getDrawable(
				android.R.color.black));
		if (savedInstanceState == null) {
			Fragment frag = new FragmentCategoriesMain();
			this.addFragment(frag);
			Syncing sync = new Syncing(this);
			if (sync.isOnline()) {
				sync.DownLoadKeywords(true);
			}

		}
	}

	@Override
	public void onStart() {
		super.onStart();
		this.startDrawerList();

	}

	@Override
	protected void onRestart() {
		super.onRestart();

		SharedPreferences prefs = this.getSharedPreferences("calificacion",
				Context.MODE_PRIVATE);
		if (prefs.getBoolean("calificar", false)) {
			FragmentListSites myFragment = (FragmentListSites) getSupportFragmentManager()
					.findFragmentByTag(ID_THIRD_FRAGMENT);
			if (prefs.getString("tag", null).equals("1")) {
				if (myFragment != null) {
					if (myFragment.isVisible()) {
						myFragment.reloadListSite(
								prefs.getString("shareParameter", null), null);
						SharedPreferences.Editor editor = prefs.edit();
						editor.clear();
						editor.commit();
					}
				}

			} else if (prefs.getString("tag", null).equals("2")) {
				if (myFragment != null) {
					if (myFragment.isVisible()) {
						myFragment.reloadListSite(null,
								prefs.getString("shareParameter", null));
						SharedPreferences.Editor editor = prefs.edit();
						editor.clear();
						editor.commit();
					}
				}

			}
		}

	}

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		drawerToggle.syncState();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		drawerToggle.onConfigurationChanged(newConfig);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.main, menu);
		MenuItem searchItem = menu.findItem(R.id.search);
		SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
		searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
		searchView.setSearchableInfo(searchManager
				.getSearchableInfo(getComponentName()));
		searchView.setIconifiedByDefault(true);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (drawerToggle.onOptionsItemSelected(item)) {
			return true;
		}
		switch (item.getItemId()) {
		case R.id.search:
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		boolean menuOpen = drawerLayout.isDrawerOpen(drawerList);
		if (menuOpen)
			menu.findItem(R.id.search).setVisible(false);
		else
			menu.findItem(R.id.search).setVisible(true);

		return super.onPrepareOptionsMenu(menu);
	}

	@Override
	public void onClickListenerItemGridView(int position, String name) {
		Bundle bundle = new Bundle();
		bundle.putInt(POSITION, position);
		bundle.putString(NAME, name);
		Fragment frag = new FragmentSubCategoriesMain();
		frag.setArguments(bundle);
		this.replaceFragment(frag, ID_SECOND_FRAGMENT);
	}

	@Override
	public void onClickListenerItem(int position, String text) {

		Bundle bundle = new Bundle();
		bundle.putString(QUERY_SEARCH, "null");
		bundle.putInt(POSITION, position);
		bundle.putString(TEXT, text);
		Fragment frag = new FragmentListSites();
		frag.setArguments(bundle);
		this.replaceFragment(frag, ID_THIRD_FRAGMENT);
	}

	@Override
	public void onClickItemListSiteListener(Site site) {
		boolean SupportLandDetails;
		Configuration config = getResources().getConfiguration();
		if (this.isTablet(this)) {
			if (config.orientation == Configuration.ORIENTATION_LANDSCAPE) {
				SupportLandDetails = true;
			} else {
				SupportLandDetails = false;
			}

		} else {
			if (config.orientation == Configuration.ORIENTATION_PORTRAIT) {
				SupportLandDetails = false;
			} else {
				SupportLandDetails = false;
			}

		}

		if (SupportLandDetails) {
			this.startFragmentDetails(site);
		} else {
			Intent intent = new Intent(this, DetailsActivity.class);
			intent.putExtras(this.fillBundleDetailSite(site));
			this.startActivity(intent);
		}
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int postition,
			long arg3) {
		Intent i = new Intent();
		switch (postition) {
		case 0:
			if (!isCurrent) {
				Fragment frag = new FragmentCategoriesMain();
				this.replaceFragment(frag, ID_FIRST_FRAGMENT);

			}
			drawerLayout.closeDrawer(drawerList);
			break;
		case 1:
			i.setClass(this, MapActivity.class);
			this.startActivity(i);
			break;
		case 2:
			i.setClass(this, CallActivity.class);
			this.startActivity(i);
			this.overridePendingTransition(R.anim.slide_in_right,
					R.anim.slide_out_left);
			break;
		case 3:

			Intent sendIntent = new Intent();
			sendIntent.setAction(Intent.ACTION_SEND);
			sendIntent.putExtra(Intent.EXTRA_TEXT,
					this.getString(R.string.app_share) + " "
							+ "http://pereira.travel/");
			sendIntent.setType("text/plain");
			startActivity(Intent.createChooser(sendIntent, getResources()
					.getText(R.string.share)));
			break;

		case 4:
			i.setClass(this, EventActivity.class);
			this.startActivity(i);
			break;

		default:
			break;
		}

	}

	@Override
	protected void onNewIntent(Intent intent) {
		this.setIntent(intent);

		handleIntent(intent);
	}

	/* Metodo que controla el boton atras */
	@Override
	public boolean onKeyUp(int keyCode, KeyEvent event) {
		if (isCurrent) {
			if (keyCode == KeyEvent.KEYCODE_BACK) {
				this.finish();
			}
			return true;
		}
		return super.onKeyUp(keyCode, event);
	}

	/* get result action bar search icon and then call method of result */
	private void handleIntent(Intent intent) {
		getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
			String query = intent.getStringExtra(SearchManager.QUERY);
			this.fragmentResultSearch(query);
		}
	}

	/* draw a list on menuDrawer */
	private void startDrawerList() {
		ObjectMenuDrawer objectsList;
		ArrayList<ObjectMenuDrawer> objList = new ArrayList<ObjectMenuDrawer>();
		AdapterMenuDrawer adapterMenuDrawer;

		objectsList = new ObjectMenuDrawer(R.drawable.categories,
				this.getString(R.string.stCategories));
		objList.add(objectsList);
		objectsList = new ObjectMenuDrawer(R.drawable.map_drawer,
				this.getString(R.string.stNearby));
		objList.add(objectsList);
		objectsList = new ObjectMenuDrawer(R.drawable.call,
				this.getString(R.string.stEmergency));
		objList.add(objectsList);
		objectsList = new ObjectMenuDrawer(R.drawable.share_drawer,
				this.getString(R.string.stShare));
		objList.add(objectsList);
		objectsList = new ObjectMenuDrawer(R.drawable.ic_event,
				this.getString(R.string.stEvent));
		objList.add(objectsList);
		Intent homeIntent = new Intent(this, Main.class);
		homeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		startActivity(homeIntent);

		adapterMenuDrawer = new AdapterMenuDrawer(this, objList);
		drawerList.setAdapter(adapterMenuDrawer);
		adapterMenuDrawer.notifyDataSetChanged();

		drawerList.setDividerHeight(1);

		drawerToggle = new ActionBarDrawerToggle(this, drawerLayout,
				R.drawable.ic_navigation_drawer, R.string.drawer_open,
				R.string.drawer_close) {
			public void onDrawerClosed(View view) {
				ActivityCompat.invalidateOptionsMenu(Main.this);
			}

			public void onDrawerOpened(View drawerView) {
				ActivityCompat.invalidateOptionsMenu(Main.this);
			}
		};
		drawerLayout.setDrawerListener(drawerToggle);
		getActionBar().setDisplayHomeAsUpEnabled(true);
		getActionBar().setHomeButtonEnabled(true);
	}

	@Override
	public void notifyDataSetChanged() {
		isCurrent = true;

	}

	@Override
	public void notifyDataSetChangedListSites() {
		isCurrent = false;

	}

	@Override
	public void notifyDataSetChangedSubCategories() {
		isCurrent = false;

	}

	@Override
	public void notifyStoredKeyWord(List<String> ListKeyWords) {
		this.fillSearchRecentSuggestions(ListKeyWords);
	}

	/* fill provider store of recent suggestions */
	private void fillSearchRecentSuggestions(List<String> listKeyWords) {
		SearchRecentSuggestions suggestions = new SearchRecentSuggestions(this,
				MySuggestionProvider.AUTHORITY, MySuggestionProvider.MODE);
		suggestions.clearHistory();
		for (String suggestion : listKeyWords) {
			suggestions.saveRecentQuery(suggestion, null);
		}
	}

	/* inflate a fragment to FrameLayout */
	private void replaceFragment(Fragment fragment, String id) {
		FragmentTransaction transaction = getSupportFragmentManager()
				.beginTransaction();
		transaction.replace(R.id.content_fragments, fragment, id);//
		transaction.addToBackStack(id);
		transaction.commit();

	}

	private void addFragment(Fragment fragment) {
		FragmentTransaction transaction = getSupportFragmentManager()
				.beginTransaction();
		transaction.add(R.id.content_fragments, fragment);
		transaction.commit();

	}

	/* inflate fragment ListSite corresponding to query search */
	private void fragmentResultSearch(String query) {

		FragmentListSites frag = new FragmentListSites();
		Bundle bundle = new Bundle();
		bundle.putString(QUERY_SEARCH, query);
		bundle.putInt(POSITION, -1);
		if (!frag.isVisible()) {
			this.replaceFragment(frag, ID_THIRD_FRAGMENT);
			frag.setArguments(bundle);
		} else {
			frag.setParameterToSearch(query);
		}

	}

	/* check whether device is tablet */
	private boolean isTablet(Context context) {
		int screenlayout = context.getResources().getConfiguration().screenLayout;
		int sizeMask = Configuration.SCREENLAYOUT_SIZE_MASK;
		int sizeLarge = Configuration.SCREENLAYOUT_SIZE_LARGE;
		boolean result = (screenlayout & sizeMask) >= sizeLarge;
		return result;
	}

	/* Start fragment that contain Detail activity with a specific Site */
	private void startFragmentDetails(Site site) {
		FragmentDetails fragDetails = new FragmentDetails();
		Fragment fragment = fragDetails;
		fragment.setArguments(this.fillBundleDetailSite(site));
		FragmentTransaction transaction = getSupportFragmentManager()
				.beginTransaction();
		transaction.replace(R.id.content_fragment_details, fragment, null);//
		transaction.commit();

	}

	private Bundle fillBundleDetailSite(Site site) {
		Configuration config = getResources().getConfiguration();
		boolean is = false;
		if (this.isTablet(this)) {
			if (config.orientation == Configuration.ORIENTATION_PORTRAIT) {
				is = true;
			}
		}
		Bundle bundle = new Bundle();
		bundle.putBoolean(SITEMAP, is);
		bundle.putInt(SITEID, site.getId());
		bundle.putString(LOGO, site.getLogo());
		bundle.putString(FULL_RATINGS, site.getFullRatings());
		bundle.putString(NAME, site.getName());
		bundle.putString(ADDRESS, site.getAddress());
		bundle.putString(PHONENUMBRE, site.getPhoneNumbre());
		bundle.putString(URL, site.getURL());
		bundle.putString(EMAIL, site.getEmail());
		bundle.putString(LAT, site.getLatitude());
		bundle.putString(LON, site.getLongitude());
		bundle.putString(DESCRIPTION, site.getDescription());
		bundle.putStringArrayList(IMAGES,
				(ArrayList<String>) site.getImagePath());

		return bundle;
	}

	@Override
	public void onShowInfoListener() {
		// No delete
	}

}
