package pereira.travel;

import java.util.ArrayList;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.view.Window;
import controls.FragmentPagerAdapterImage;
import controls.ScreenSlidePageFragment;

public class ImageViewActivity extends FragmentActivity {

	ViewPager pager = null;
	FragmentPagerAdapterImage pagerAdapter;
	ArrayList<String> imageList;
	int index;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_image_view);
		this.pager = (ViewPager) this.findViewById(R.id.pager);
		Bundle b = this.getIntent().getBundleExtra(GaleryActivity.POS);
		imageList = b.getStringArrayList(GaleryActivity.GALERY);
		index = b.getInt(GaleryActivity.POS);
		this.setTitle(null);

	}

	@Override
	protected void onStart() {
		super.onStart();
		ArrayList<ScreenSlidePageFragment> fragmentList = new ArrayList<ScreenSlidePageFragment>();
		ScreenSlidePageFragment frag;
		for (String imagepath : imageList) {
			frag = new ScreenSlidePageFragment();
			frag.setImage(imagepath);
			fragmentList.add(frag);

		}
		FragmentPagerAdapterImage adapter = new FragmentPagerAdapterImage(
				getSupportFragmentManager(), fragmentList);
		this.pager.setAdapter(adapter);
		this.pager.setCurrentItem(index);

	}

}
