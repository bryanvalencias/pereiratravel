package pereira.travel;

import java.util.ArrayList;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import controls.AdapterGallery;

public class GaleryActivity extends Activity implements OnItemClickListener {
	private GridView gridView1;
	private ArrayList<String> imageList;
	public static String GALERY = "galery";
	public static String POS = "position";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_galery);
		this.gridView1 = (GridView) this.findViewById(R.id.gridView1);
		getActionBar().setDisplayHomeAsUpEnabled(true);
		getActionBar().setHomeButtonEnabled(true);
		this.setTitle(null);
		ActionBar bar = getActionBar();
		bar.setBackgroundDrawable(getResources().getDrawable(
				android.R.color.black));

	}

	@Override
	protected void onStart() {
		super.onStart();
		Bundle bundle = this.getIntent().getBundleExtra(GALERY);
		imageList = bundle.getStringArrayList(GALERY);
		Log.e("imageListSize", String.valueOf(imageList.size()));
		AdapterGallery adapter = new AdapterGallery(this, imageList);
		this.gridView1.setAdapter(adapter);
		this.gridView1.setOnItemClickListener(this);

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem menuItem) {

		switch (menuItem.getItemId()) {
		case android.R.id.home:
			Configuration config = getResources().getConfiguration();
			if (config.orientation == Configuration.ORIENTATION_PORTRAIT) {
				this.finish();
			} else {
				Intent homeIntent = new Intent(this, Main.class);
				homeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				this.startActivity(homeIntent);
			}
			return true;
		}
		return (super.onOptionsItemSelected(menuItem));
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int pos, long arg3) {
		Bundle b = new Bundle();
		b.putStringArrayList(GALERY, imageList);
		b.putInt(POS, pos);
		Intent i = new Intent();
		i.setClass(this, ImageViewActivity.class);
		i.putExtra(POS, b);
		this.startActivity(i);
	}

}
