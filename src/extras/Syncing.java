package extras;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import model.Article;
import model.Category;
import model.Site;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.xmlpull.v1.XmlPullParserException;

import pereira.travel.Main;
import pereira.travel.R;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.StatFs;
import android.util.Log;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.MemoryCacheUtil;

public class Syncing {

	public static String URL = "http://www.pereira.travel/ws/Panel/webserviceslugares.php";
	public static String nameUser = "movilAppUser";
	public static String pass = "1659b9f797ab61bd0cc5929a4ac22dcc";
	public static String keyWs = "5622989041371af9966889cf2b9fb0da";
	public static String RADIO = "1000";
	public static String requestMethodCategoryList = "consultarListaCategoriasLugares";
	public static String requestMethodSitesList = "consultarListaLugares";
	public static String requestMethodkeyWords = "consultarListaPalabrasClave";
	public static String requestMethodNearby = "consultarListaLugaresCercanos";
	public static String requestMethodRakings = "enviarCalificacionLugar";
	public static String requestMethodArticle = "consultarListaArticulos";
	public static boolean outMemory = false;

	private Context ctx;
	private List<Category> listCategory;
	private List<String> listKeyWords;
	private List<Site> listSite;
	public int position;

	XMLParserHelper XmlParserHelper;
	public OnIsStoredListener listenerIsStored;
	public OnReturnArraySubCategoriesListener listenerReturnList;
	public OnGetListSitesListener listenerGetSitesList;
	public OnGetListKeyWordsListener listenerGetKeyWordList;
	public OnGetListSitesListenerNear listenerGetSitesListNear;
	public OnResponseRakings listenerResponseRakings;
	public OnGetListArticleListener listenerArticle;

	public Syncing(Context context) {
		this.ctx = context;
		this.listCategory = null;
		this.listKeyWords = null;
		this.listSite = null;

	}

	public Syncing(Context context, OnResponseRakings listener) {
		this.ctx = context;
		this.listenerResponseRakings = listener;
	}

	public Syncing(Context context, OnGetListArticleListener listener) {
		this.ctx = context;
		this.listenerArticle = listener;
	}

	public Syncing(Context context, OnGetListKeyWordsListener listener) {
		this.ctx = context;
		this.listCategory = null;
		this.listKeyWords = null;
		this.listSite = null;
		this.listenerGetKeyWordList = listener;
	}

	public Syncing(Context context, OnIsStoredListener listener,
			OnGetListKeyWordsListener listenerStoredkeyword) {
		this.ctx = context;
		this.listCategory = null;
		this.listKeyWords = null;
		this.listSite = null;
		this.listenerIsStored = (OnIsStoredListener) listener;
		this.listenerGetKeyWordList = listenerStoredkeyword;

	}

	public Syncing(Context context, OnIsStoredListener listener,
			OnGetListSitesListener listenerGetList) {
		this.ctx = context;
		this.listCategory = null;
		this.listKeyWords = null;
		this.listSite = null;
		this.listenerIsStored = (OnIsStoredListener) listener;
		this.listenerGetSitesList = listenerGetList;

	}

	public Syncing(Context context, OnIsStoredListener listener,
			OnGetListSitesListener listenerGetList,
			OnGetListSitesListenerNear listenerNear) {
		this.ctx = context;
		this.listCategory = null;
		this.listKeyWords = null;
		this.listSite = null;
		this.listenerIsStored = (OnIsStoredListener) listener;
		this.listenerGetSitesList = listenerGetList;
		this.listenerGetSitesListNear = listenerNear;

	}

	public Syncing(Context context, int position,
			OnReturnArraySubCategoriesListener listenerReturnList) {
		this.ctx = context;
		this.listCategory = null;
		this.listKeyWords = null;
		this.listSite = null;
		this.position = position;
		this.listenerReturnList = listenerReturnList;

	}

	public interface OnIsStoredListener {
		public void onDataStoredListener(List<Category> categoryList);

		public void onMessageError(String text);

	}

	public interface OnReturnArraySubCategoriesListener {
		public void onReturnArraySubCategoriesListener(
				ArrayList<Category> listCategory);
	}

	public interface OnGetListSitesListener {
		public void onGetListSiteListener(List<Site> listSite);
	}

	public interface OnGetListSitesListenerNear {
		public void onGetListSiteListenerNear(List<Site> listSite);
	}

	public interface OnNotifyDataSetChanged {
		public void notifyDataSetChanged();
	}

	public interface OnGetListKeyWordsListener {
		public void onGetListKeyWordsListener(List<String> listKeyWords);
	}

	public interface OnGetListArticleListener {
		public void onGetListArticleListener(List<Article> listArticle);

		public void onGetErrorListArticleListener(String Error);
	}

	public interface OnResponseRakings {
		public void onGetResponseRakings(String[] result);

		public void onGetResponseErrorRakings(String result);
	}

	public void DownLoad(boolean online, String all) {
		DownLoadTaskListCategories down = new DownLoadTaskListCategories();
		String param = "-1";
		if (online) {
			down.execute(URL, Main.ONLINE, null, param);
		} else {
			down.execute(URL, Main.OFFLINE, all, param);
		}
	}

	public void DownLoadListSitesNear(String Lat, String Long, String Radio,
			String idCategory) {
		PostParameterToGetXmlSitesNear down = new PostParameterToGetXmlSitesNear();
		down.execute(URL, Lat, Long, Radio, idCategory);

	}

	public void DownLoadListArticle(boolean online, String url,
			String idCategoryArticle, String numArticle) {
		// params[0]= online.
		// params[1]= url.
		// params[2]= idcategoryArticle.
		// params[3]= numArticle.

		downLoadArticle down = new downLoadArticle();

		if (online) {
			down.execute(Main.ONLINE, URL, idCategoryArticle, numArticle);
		} else {
			down.execute(Main.OFFLINE, URL, idCategoryArticle, numArticle);
		}
	}

	public void DownLoadKeywords(boolean online) {
		DownLoadTaskListKeywords down = new DownLoadTaskListKeywords();

		if (online) {
			down.execute(URL, Main.ONLINE);
		} else {
			down.execute(URL, Main.OFFLINE);
		}
	}

	public void downLoadSites(String idFilter, String keyword, String idSite) {
		PostParameterToGetXmlSites post = new PostParameterToGetXmlSites();
		post.execute(URL, idFilter, keyword, idSite);
	}

	public void getResponseRankings(String idsite, String valueRatings,
			String idPhone) {
		setRakingsToServer down = new setRakingsToServer();
		down.execute(URL, idsite, valueRatings, idPhone);
	}

	public void initGetSubCategory() {
		GetSubCategory down = new GetSubCategory();
		down.execute();
	}

	public boolean isOnline() {
		ConnectivityManager connMgr = (ConnectivityManager) ctx
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
		return (networkInfo != null && networkInfo.isConnected());
	}

	private void loadXmlFromNetwork(String idCategory, String urlString)
			throws XmlPullParserException, IOException {
		InputStream stream = null;
		XmlParserHelper = new XMLParserHelper();
		try {
			stream = getResponseListCategory(idCategory, urlString);
			listCategory = XmlParserHelper.parseDataCategory(stream);
		} finally {
			if (stream != null) {
				stream.close();
			}
		}

	}

	private void loadXmlFromNetwork(String urlString)
			throws XmlPullParserException, IOException {
		InputStream stream = null;
		XmlParserHelper = new XMLParserHelper();
		try {
			stream = getResponseKeywords(urlString);
			listKeyWords = XmlParserHelper.parseDataKeywords(stream);
		} finally {
			if (stream != null) {
				stream.close();
			}
		}

	}

	public List<Category> getListCategory() {
		return listCategory;
	}

	public List<Category> getListCategory(boolean Connexion) {
		this.DownLoad(Connexion, null);
		return listCategory;
	}

	public List<Category> getListCategory(boolean Connexion, String all) {
		this.DownLoad(Connexion, all);
		return listCategory;
	}

	public List<Site> getListSites() {
		return listSite;
	}

	/*
	 * Post Xml parameters and data login and then return String Xml with result
	 * state and category list
	 */
	private InputStream getResponseListCategory(String idCategory, String url)
			throws IllegalArgumentException, IllegalStateException, IOException {
		XmlParserHelper = new XMLParserHelper();
		HttpClient httpclient = new DefaultHttpClient();
		HttpPost httppost = new HttpPost(url);

		String response = XmlParserHelper.buildXmlGetListCategory(nameUser,
				pass, keyWs, requestMethodCategoryList, idCategory);

		StringEntity se = new StringEntity(response, "ISO-8859-1");
		httppost.setEntity(se);
		se.setContentType("application/atom+xml");
		HttpResponse responseXml = httpclient.execute(httppost);
		String inputStream = EntityUtils.toString(responseXml.getEntity());
		Log.e("Response-->ListCategory", inputStream);
		InputStream stream = new ByteArrayInputStream(
				inputStream.getBytes("ISO-8859-1"));
		return stream;
	}

	/*
	 * Post Xml parameters and data login and then return String Xml with result
	 * state and sites list
	 */
	private InputStream getResponseListSites(String idCategory, String url,
			String keyword, String idSite) throws IllegalArgumentException,
			IllegalStateException, IOException {
		SharedPreferences prefs = ctx.getSharedPreferences("calificacion",
				Context.MODE_PRIVATE);
		XmlParserHelper = new XMLParserHelper();
		HttpClient httpclient = new DefaultHttpClient();
		HttpPost httppost = new HttpPost(url);
		String response = null;
		if (idCategory != null) {
			response = XmlParserHelper.buildXmlGetListSites(nameUser, pass,
					keyWs, requestMethodSitesList, idCategory, null, null);
			SharedPreferences.Editor editor = prefs.edit();
			editor.putString("shareParameter", idCategory);
			editor.putBoolean("calificar", false);
			editor.putString("tag", "1");
			editor.commit();
			Log.e("ListSites-->idCategory", idCategory);
			Log.e("shared_saved",
					String.valueOf(prefs.getBoolean("calificar", false)));

		} else if (keyword != null) {
			response = XmlParserHelper.buildXmlGetListSites(nameUser, pass,
					keyWs, requestMethodSitesList, null, keyword, null);
			Log.e("ListSites-->keyword", keyword);
			SharedPreferences.Editor editor = prefs.edit();
			editor.putString("shareParameter", keyword);
			editor.putBoolean("calificar", false);
			editor.putString("tag", "2");
			editor.commit();
			Log.e("shared_saved",
					String.valueOf(prefs.getBoolean("calificar", false)));

		} else if (idSite != null) {
			// only module maps
			response = XmlParserHelper.buildXmlGetListSites(nameUser, pass,
					keyWs, requestMethodSitesList, null, null, idSite);

			Log.e("ListSites-->idSite", idSite);
		}
		StringEntity se = new StringEntity(response, "ISO-8859-1");
		httppost.setEntity(se);
		se.setContentType("application/atom+xml");
		HttpResponse responseXml = httpclient.execute(httppost);
		String inputStream = EntityUtils.toString(responseXml.getEntity());
		Log.e("Response-->ListSites", inputStream);

		InputStream stream = new ByteArrayInputStream(
				inputStream.getBytes("ISO-8859-1"));
		return stream;
	}

	/*
	 * Post Xml parameters and data login and then return String Xml with result
	 * state and sitesNear list
	 */
	private InputStream getResponseListSitesNear(String url, String Lat,
			String Long, String Radio, String idCategory)
			throws IllegalArgumentException, IllegalStateException, IOException {
		XmlParserHelper = new XMLParserHelper();
		HttpClient httpclient = new DefaultHttpClient();
		HttpPost httppost = new HttpPost(url);
		String response = null;
		response = XmlParserHelper.buildXmlNearby(nameUser, pass, keyWs,
				requestMethodNearby, Lat, Long, Radio, idCategory);
		StringEntity se = new StringEntity(response, "ISO-8859-1");
		httppost.setEntity(se);
		se.setContentType("application/atom+xml");
		HttpResponse responseXml = httpclient.execute(httppost);
		String inputStream = EntityUtils.toString(responseXml.getEntity());
		Log.e("Response-->ListSitesNear", inputStream);
		InputStream stream = new ByteArrayInputStream(
				inputStream.getBytes("ISO-8859-1"));
		return stream;
	}

	/*
	 * Post Xml parameters and data login and then return String Xml with result
	 * state and sitesNear list
	 */
	private InputStream getResponseRatings(String url, String idsite,
			String valueRating, String idPhone)
			throws IllegalArgumentException, IllegalStateException, IOException {
		XmlParserHelper = new XMLParserHelper();
		HttpClient httpclient = new DefaultHttpClient();
		HttpPost httppost = new HttpPost(url);
		String response = null;
		response = XmlParserHelper.buildXmlsetRatings(nameUser, pass, keyWs,
				requestMethodRakings, idsite, valueRating, idPhone);
		StringEntity se = new StringEntity(response, "ISO-8859-1");
		httppost.setEntity(se);
		se.setContentType("application/atom+xml");
		HttpResponse responseXml = httpclient.execute(httppost);
		String inputStream = EntityUtils.toString(responseXml.getEntity());
		Log.e("Response-->ratings", inputStream);
		InputStream stream = new ByteArrayInputStream(
				inputStream.getBytes("ISO-8859-1"));
		return stream;
	}

	/*
	 * Post Xml parameters and data login and then return String Xml with result
	 * state and sitesNear list
	 */
	private InputStream getResponseArticle(String url,
			String idCategoryArticle, String numArticle)
			throws IllegalArgumentException, IllegalStateException, IOException {
		XmlParserHelper = new XMLParserHelper();
		HttpClient httpclient = new DefaultHttpClient();
		HttpPost httppost = new HttpPost(url);
		String response = null;
		response = XmlParserHelper.buildXmlgetArticle(nameUser, pass, keyWs,
				requestMethodArticle, idCategoryArticle, numArticle);
		StringEntity se = new StringEntity(response, "ISO-8859-1");
		httppost.setEntity(se);
		se.setContentType("application/atom+xml");
		HttpResponse responseXml = httpclient.execute(httppost);
		String inputStream = EntityUtils.toString(responseXml.getEntity());
		Log.e("Response-->article", inputStream);
		InputStream stream = new ByteArrayInputStream(
				inputStream.getBytes("ISO-8859-1"));
		return stream;
	}

	/*
	 * Post Xml parameters and data login and then return String Xml with result
	 * state and site details
	 */
	private InputStream getResponseKeywords(String url)
			throws IllegalArgumentException, IllegalStateException, IOException {
		XmlParserHelper = new XMLParserHelper();
		HttpClient httpclient = new DefaultHttpClient();
		HttpPost httppost = new HttpPost(url);
		String response = XmlParserHelper.buildXmlGetkeywords(nameUser, pass,
				keyWs, requestMethodkeyWords);
		StringEntity se = new StringEntity(response, "ISO-8859-1");
		httppost.setEntity(se);
		se.setContentType("application/atom+xml");
		HttpResponse responseXml = httpclient.execute(httppost);
		String inputStream = EntityUtils.toString(responseXml.getEntity());
		Log.e("Response--> keyWords", inputStream);
		InputStream stream = new ByteArrayInputStream(
				inputStream.getBytes("ISO-8859-1"));
		return stream;
	}

	private List<Site> getListSiteFromParseSite(String url,
			String idRootCategory, String keyword, String idSite)
			throws ClientProtocolException, IOException,
			XmlPullParserException, IllegalStateException {
		InputStream stream = getResponseListSites(idRootCategory, url, keyword,
				idSite);
		this.listSite = XmlParserHelper.parseDataSite(stream);
		for (Site site : listSite) {
			Log.e("Site", site.getName() + "--" + site.getAddress() + "--"
					+ site.getEmail() + "--" + site.getImagePath());
		}
		if (this.listSite.size() == 0) {
			return null;
		} else {
			return this.listSite;
		}
	}

	private List<Site> getListSiteFromParseSiteNear(String url, String Lat,
			String Long, String Radio, String idCategory)
			throws ClientProtocolException, IOException,
			XmlPullParserException, IllegalStateException {
		InputStream stream = getResponseListSitesNear(url, Lat, Long, Radio,
				idCategory);
		this.listSite = XmlParserHelper.parseDataSiteNear(stream);
		for (Site site : listSite) {
			Log.e("SiteNear", site.getId() + "--" + site.getName() + "--"
					+ site.getLatitude() + "--" + site.getLongitude());
		}
		if (this.listSite.size() == 0) {
			return null;
		} else {
			return this.listSite;
		}
	}

	private String[] getRankingsFromParse(String url, String idsite,
			String valueRating, String idPhone) throws ClientProtocolException,
			IOException, XmlPullParserException, IllegalStateException {
		InputStream stream = getResponseRatings(url, idsite, valueRating,
				idPhone);
		String[] result = XmlParserHelper.parseRatings(stream);
		return result;
	}

	private List<Article> getArticleFromParse(String url,
			String idCategoryArticle, String numArticle)
			throws ClientProtocolException, IOException,
			XmlPullParserException, IllegalStateException {
		InputStream stream = getResponseArticle(url, idCategoryArticle,
				numArticle);
		List<Article> result = XmlParserHelper.parseArticle(stream);
		return result;
	}

	@SuppressWarnings("deprecation")
	public static String getAvailableInternalMemorySize() {
		File path = Environment.getDataDirectory();
		StatFs stat = new StatFs(path.getPath());
		long blockSize = stat.getBlockSize();
		long availableBlocks = stat.getAvailableBlocks();
		return formatSize(availableBlocks * blockSize);
	}

	private static String formatSize(long size) {
		String suffix = null;

		if (size >= 1024) {
			suffix = "KB";
			size /= 1024;
			if (size >= 1024) {
				suffix = "MB";
				size /= 1024;
				if (size < 15) {
					outMemory = true;
				}
			}
		}

		StringBuilder resultBuffer = new StringBuilder(Long.toString(size));

		int commaOffset = resultBuffer.length() - 3;
		while (commaOffset > 0) {
			resultBuffer.insert(commaOffset, ',');
			commaOffset -= 3;
		}

		if (suffix != null)
			resultBuffer.append(suffix);
		return resultBuffer.toString();
	}

	public class GetSubCategory extends AsyncTask<Void, Void, List<Category>> {
		PersistenceDataBase database = new PersistenceDataBase(ctx);
		ProgressDialog pd;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pd = ProgressDialog.show(ctx, null, ctx.getString(R.string.Loading)
					+ "...", true, true);
		}

		@Override
		protected List<Category> doInBackground(Void... params) {
			listCategory = null;
			database.Open();
			listCategory = database.getArrayAllCategories();
			ArrayList<Integer> idRootList = new ArrayList<Integer>();
			for (Category category : listCategory) {
				idRootList.add(category.getIdCategory());
			}
			listCategory = database.getArraySubCategories(idRootList
					.get(position));
			database.Close();
			return listCategory;
		}

		@Override
		protected void onPostExecute(List<Category> result) {
			super.onPostExecute(result);
			if (result != null) {
				if (listenerReturnList != null) {
					listenerReturnList
							.onReturnArraySubCategoriesListener((ArrayList<Category>) result);
				}
			}
			pd.dismiss();
		}
	}

	public class DownLoadTaskListKeywords extends
			AsyncTask<String, Object, Integer> {
		PersistenceDataBase database = new PersistenceDataBase(ctx);
		int now = -1;

		private void getAllKeyWordsFromDataBase() {
			listKeyWords = null;
			listKeyWords = database.getArrayAllkeyWords();
		}

		@Override
		protected Integer doInBackground(String... dataInit) {
			try {
				database.Open();
				if (dataInit[1].equals(Main.ONLINE)) {
					if (database.getCountKeyWords() == 0) {
						loadXmlFromNetwork(dataInit[0]);
						if (listKeyWords != null) {
							Log.e("is listKeyWords null",
									"listKeyWords is not null");
							database.registerKeyWordsToDb(listKeyWords);
							this.getAllKeyWordsFromDataBase();
						}
						return 1;
					} else {
						// Data base is not empty
						Log.e("REGISTERKEYWORDSTODB",
								"Database KEYWORDS is not empty");
						this.getAllKeyWordsFromDataBase();
						this.publishProgress(listKeyWords);
						loadXmlFromNetwork(dataInit[0]);
						if (listKeyWords != null) {
							int countAffected = database
									.deleteKeyWordsToDatabase();
							if (countAffected != 0) {
								Log.e("DATA_DELETED", "Deleted "
										+ countAffected + " rows");
								database.registerKeyWordsToDb(listKeyWords);
								Log.e("is listKeyWords null",
										"LISTKEYWORDS is not null");

							}
						}
						return 2;
					}
				} else if (dataInit[1].equals(Main.OFFLINE)) {
					this.getAllKeyWordsFromDataBase();
					return 2;
				}
			} catch (IOException e) {
				Log.e("IOException", e.getMessage());
				return -1;
			} catch (XmlPullParserException e) {
				Log.e("XmlPullParserException", e.getMessage());
				return -2;
			} finally {
				database.Close();
			}
			return 0;

		}

		@SuppressWarnings("unchecked")
		@Override
		protected void onProgressUpdate(Object... progress) {
			super.onProgressUpdate(progress);

			if (listenerGetKeyWordList != null) {
				if (progress[0] != null) {
					listenerGetKeyWordList
							.onGetListKeyWordsListener((List<String>) progress[0]);
					listenerGetKeyWordList = null;
				}
			}

		}

		@Override
		protected void onPostExecute(Integer result) {
			super.onPostExecute(result);

			if (listenerGetKeyWordList != null) {
				listenerGetKeyWordList.onGetListKeyWordsListener(listKeyWords);
			} else {
				Log.e("NUll", "listenerGetKeyWordList is null");
			}

			if (now == -1) {
				switch (result) {
				case 0:
					Log.e("UpdateKeywords 0",
							ctx.getString(R.string.DownLoadedKeywords));
					// Toast.makeText(ctx,
					// ctx.getString(R.string.DownLoadedKeywords),
					// Toast.LENGTH_SHORT).show();
					break;
				case 1:
					Log.e("UpdateKeywords 1",
							ctx.getString(R.string.DownLoadedKeywords));
					Toast.makeText(ctx,
							ctx.getString(R.string.DownLoadedKeywords),
							Toast.LENGTH_SHORT).show();
					break;
				case 2:
					Log.e("UpdateKeywords 2",
							ctx.getString(R.string.DownLoadedKeywords));
					// Toast.makeText(ctx,
					// ctx.getString(R.string.DownLoadedKeywords),
					// Toast.LENGTH_SHORT).show();
					break;
				case -1:
					Toast.makeText(ctx,
							ctx.getString(R.string.ErrorIOException),
							Toast.LENGTH_LONG).show();
					if (listenerIsStored != null) {

						listenerIsStored.onMessageError(ctx
								.getString(R.string.ErrorMessageResult));
					}

					break;
				case -2:
					Toast.makeText(
							ctx,
							ctx.getString(R.string.ErrorXmlPullParserException),
							Toast.LENGTH_LONG).show();
					if (listenerIsStored != null) {
						listenerIsStored.onMessageError(ctx
								.getString(R.string.ErrorMessageResult));
					}
					break;

				default:

					break;
				}

			}
		}
	}

	public class DownLoadTaskListCategories extends
			AsyncTask<String, Object, Integer> {
		int now = -1;
		PersistenceDataBase database = new PersistenceDataBase(ctx);

		private void getCategoriesFromDataBase() {
			listCategory = null;
			listCategory = database.getArrayCategories();
		}

		private void getAllCategoriesFromDataBase() {
			listCategory = null;
			listCategory = database.getArrayAllCategories();
		}

		@Override
		protected Integer doInBackground(String... dataInit) {
			try {
				database.Open();
				if (dataInit[1].equals(Main.ONLINE)) {

					if (database.getCountCategories() == 0) {
						// Data base is empty
						Log.e("REGISTERCATEGORIESTODB", "Data base is empty");
						loadXmlFromNetwork(dataInit[3], dataInit[0]);
						if (listCategory != null) {
							Log.e("is listCategory null",
									"listCategory is not null");
							database.registerCategoriesToDb(listCategory);
							this.getCategoriesFromDataBase();
						}
					} else {
						// Data base is not empty
						Log.e("REGISTERCATEGORIESTODB",
								"Database CATEGORIES is not empty");
						this.getCategoriesFromDataBase();
						this.publishProgress(listCategory);
						loadXmlFromNetwork(dataInit[3], dataInit[0]);

						if (listCategory != null) {
							int countAffected = database
									.deleteCategoriesToDatabase();
							// determines the number of rows deleted
							if (countAffected != 0) {
								Log.e("DATA_DELETED", "Deleted "
										+ countAffected + " rows");
								for (Category category : listCategory) {
									MemoryCacheUtil.removeFromCache(category
											.getImagesPath(), ImageLoader
											.getInstance().getMemoryCache());
								}
								database.registerCategoriesToDb(listCategory);
								Log.e("REGISTERCATEGORIESTODB",
										"data is registered");

							}
						}
					}

				} else if (dataInit[1].equals(Main.OFFLINE)) {

					if (dataInit[2] != null) {
						this.getAllCategoriesFromDataBase();
						if (database.getCountCategories() == 0) {
							return -1;
						}
					} else {
						this.getCategoriesFromDataBase();
						if (database.getCountCategories() == 0) {
							return -1;
						}
					}
				}
				return 0;
			} catch (IOException e) {
				Log.e("IOException", e.getMessage());
				return -1;
			} catch (XmlPullParserException e) {
				Log.e("XmlPullParserException", e.getMessage());
				return -2;
			} finally {
				database.Close();
			}
		}

		@SuppressWarnings("unchecked")
		@Override
		protected void onProgressUpdate(Object... progress) {
			super.onProgressUpdate(progress);

			if (listenerIsStored != null) {
				if (progress[0] != null) {
					listenerIsStored
							.onDataStoredListener((List<Category>) progress[0]);
					Log.e("PUBLISHPROGRESS", "data is update");
					listenerIsStored = null;
				}
			}
			now = 0;

		}

		@Override
		protected void onPostExecute(Integer result) {
			super.onPostExecute(result);

			if (listenerIsStored != null) {
				listenerIsStored.onDataStoredListener(listCategory);
			} else {
				Log.e("NUll", "listenerIsStored is null");
			}

			if (now == -1) {
				switch (result) {
				case 0:
					// Toast.makeText(ctx, ctx.getString(R.string.Ready),
					// Toast.LENGTH_SHORT).show();
					Log.e("Ready", ctx.getString(R.string.Ready));
					break;
				case -1:
					Toast.makeText(ctx,
							ctx.getString(R.string.ErrorIOException),
							Toast.LENGTH_LONG).show();
					listenerIsStored.onMessageError(ctx
							.getString(R.string.ErrorIOMessageResult));

					break;
				case -2:
					Toast.makeText(
							ctx,
							ctx.getString(R.string.ErrorXmlPullParserException),
							Toast.LENGTH_LONG).show();
					listenerIsStored.onMessageError(ctx
							.getString(R.string.ErrorMessageResult));
					break;

				default:

					break;
				}

			}
		}
	}

	public class PostParameterToGetXmlSites extends
			AsyncTask<String, Integer, List<Site>> {
		ProgressDialog pd;

		@Override
		protected List<Site> doInBackground(String... params) {
			try {

				List<Site> siteListe = getListSiteFromParseSite(params[0],
						params[1], params[2], params[3]);

				if (siteListe != null) {
					for (Site site : siteListe) {
						MemoryCacheUtil.removeFromCache(site.getLogo(),
								ImageLoader.getInstance().getMemoryCache());
						for (String imagePath : site.getImagePath()) {

							MemoryCacheUtil.removeFromCache(imagePath,
									ImageLoader.getInstance().getMemoryCache());
						}
					}
					return siteListe;
				}
			} catch (ClientProtocolException e) {
				Log.e("ClientProtocolException ", e.getMessage());
				publishProgress(1);
			} catch (IOException e) {
				Log.e("IOException ", e.getMessage());
				publishProgress(2);
			} catch (NumberFormatException e) {
				Log.e("NumberFormatException ", e.getMessage());
				publishProgress(3);
			} catch (XmlPullParserException e) {
				Log.e("XmlPullParserException ", e.getMessage());
				publishProgress(4);
			} catch (IllegalStateException e) {
				Log.e("IllegalStateException ", e.getMessage());
				publishProgress(5);
			}
			return null;
		}

		@Override
		protected void onProgressUpdate(Integer... values) {
			super.onProgressUpdate(values);
			switch (values[0]) {
			case 1:

				if (listenerIsStored != null) {
					listenerIsStored.onMessageError(ctx
							.getString(R.string.ErrorMessageResult));
				}

				break;
			case 2:

				if (listenerIsStored != null) {
					listenerIsStored.onMessageError(ctx
							.getString(R.string.ErrorIOMessageResult));
				}

				break;
			case 3:

				if (listenerIsStored != null) {
					listenerIsStored.onMessageError(ctx
							.getString(R.string.ErrorMessageResult));
				}

				break;
			case 4:

				if (listenerIsStored != null) {
					listenerIsStored.onMessageError(ctx
							.getString(R.string.ErrorMessageResult));
				}

				break;
			case 5:

				if (listenerIsStored != null) {
					listenerIsStored.onMessageError(ctx
							.getString(R.string.ErrorMessageResult));
				}

				break;

			default:

				break;
			}
			listenerGetSitesList = null;
			listenerIsStored = null;

		}

		@Override
		protected void onPostExecute(List<Site> result) {
			super.onPostExecute(result);
			if (result != null) {
				if (listenerGetSitesList != null) {
					listenerGetSitesList.onGetListSiteListener(result);
				}
			} else {
				if (listenerIsStored != null) {
					listenerIsStored.onMessageError(ctx
							.getString(R.string.ErrorNullMessageResult));
				}
			}
			pd.dismiss();

		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pd = ProgressDialog.show(ctx, null,
					ctx.getString(R.string.SearchSites) + "...", true, true);
		}
	}

	public class PostParameterToGetXmlSitesNear extends
			AsyncTask<String, Integer, Object> {

		@Override
		protected Object doInBackground(String... params) {
			try {
				// params[0]=url;
				// params[1]=Latitude;
				// params[2]=Longitude;
				// params[3]=Radio;
				// params[4]=idCategory;
				List<Site> siteListe = getListSiteFromParseSiteNear(params[0],
						params[1], params[2], params[3], params[4]);
				if (siteListe != null) {
					return siteListe;
				} else {
					if (params[4] != null) {
						return params[4];
					}
				}
			} catch (ClientProtocolException e) {
				Log.e("ClientProtocolException ", e.getMessage());
				publishProgress(1);
			} catch (IOException e) {
				Log.e("IOException ", e.getMessage());
				publishProgress(2);
			} catch (NumberFormatException e) {
				Log.e("NumberFormatException ", e.getMessage());
				publishProgress(3);
			} catch (XmlPullParserException e) {
				Log.e("XmlPullParserException ", e.getMessage());
				publishProgress(4);
			} catch (IllegalStateException e) {
				Log.e("IllegalStateException ", e.getMessage());
				publishProgress(5);
			}
			return null;
		}

		@Override
		protected void onProgressUpdate(Integer... values) {
			super.onProgressUpdate(values);
			switch (values[0]) {
			case 1:

				if (listenerIsStored != null) {
					listenerIsStored.onMessageError(ctx
							.getString(R.string.ErrorMessageResult));
				}

				break;
			case 2:

				if (listenerIsStored != null) {
					listenerIsStored.onMessageError(ctx
							.getString(R.string.ErrorIOMessageResult));
				}

				break;
			case 3:

				if (listenerIsStored != null) {
					listenerIsStored.onMessageError(ctx
							.getString(R.string.ErrorMessageResult));
				}

				break;
			case 4:

				if (listenerIsStored != null) {
					listenerIsStored.onMessageError(ctx
							.getString(R.string.ErrorMessageResult));
				}

				break;
			case 5:

				if (listenerIsStored != null) {
					listenerIsStored.onMessageError(ctx
							.getString(R.string.ErrorMessageResult));
				}

				break;

			default:

				break;
			}
			listenerGetSitesListNear = null;
			listenerIsStored = null;

		}

		@SuppressWarnings("unchecked")
		@Override
		protected void onPostExecute(Object result) {
			super.onPostExecute(result);
			if (result != null) {
				if (listenerGetSitesListNear != null) {
					if (result instanceof String) {
						listenerIsStored.onMessageError((String) result);
					} else {
						listenerGetSitesListNear
								.onGetListSiteListenerNear((ArrayList<Site>) result);
					}
				}
			} else {
				if (listenerIsStored != null) {
					listenerIsStored.onMessageError(ctx
							.getString(R.string.ErrorNullMessageResult));
				}
			}

		}
	}

	public class setRakingsToServer extends AsyncTask<String, Object, Object> {

		@Override
		protected Object doInBackground(String... params) {
			try {
				// params[0]= url.
				// params[1]= idsite.
				// params[2]= valueRatings.
				// params[3]= idPhone.
				String[] result = getRankingsFromParse(params[0], params[1],
						params[2], params[3]);

				return result;

			} catch (ClientProtocolException e) {
				return ctx.getString(R.string.ErrorMessageResult);
			} catch (IllegalStateException e) {
				return ctx.getString(R.string.ErrorIOMessageResult);
			} catch (IOException e) {
				return ctx.getString(R.string.ErrorMessageResult);
			} catch (XmlPullParserException e) {
				return ctx.getString(R.string.ErrorMessageResult);
			}
		}

		@Override
		protected void onPostExecute(Object result) {
			super.onPostExecute(result);
			if (result != null) {
				if (listenerResponseRakings != null) {
					if (result instanceof String[]) {
						listenerResponseRakings
								.onGetResponseRakings((String[]) result);

					} else if (result instanceof String) {
						listenerResponseRakings
								.onGetResponseErrorRakings((String) result);
					}
				}
			}

		}
	}

	public class downLoadArticle extends AsyncTask<String, Object, Object> {
		PersistenceDataBase database = new PersistenceDataBase(ctx);
		List<Article> listArticles;
		int count;

		@Override
		protected Object doInBackground(String... params) {
			// params[0]= online.
			// params[1]= url.
			// params[2]= idcategoryArticle.
			// params[3]= numArticle.
			try {
				database.Open();
				if (params[0].equals(Main.ONLINE)) {
					if (database.getCountArticle() == 0) {
						listArticles = getArticleFromParse(params[1],
								params[2], params[3]);
						database.registerArticleToDb(listArticles);

					} else {
						count = database.deleteArticleToDatabase();
						if (count != 0) {
							listArticles = getArticleFromParse(params[1],
									params[2], params[3]);
							database.registerArticleToDb(listArticles);
							return listArticles;
						}
					}
				} else {
					if (database.getCountArticle() != 0) {
						listArticles = database.getArrayArticle();
						for (Article art : listArticles) {
							MemoryCacheUtil.removeFromCache(art.getImage(),
									ImageLoader.getInstance().getMemoryCache());
						}
					} else {
						listArticles = null;
					}
				}
				return listArticles;

			} catch (ClientProtocolException e) {
				return null;
			} catch (IllegalStateException e) {
				return null;
			} catch (IOException e) {
				return null;
			} catch (XmlPullParserException e) {
				return null;
			} finally {
				database.Close();
			}

		}

		@SuppressWarnings("unchecked")
		@Override
		protected void onPostExecute(Object result) {
			super.onPostExecute(result);
			if (result != null) {
				listenerArticle
						.onGetListArticleListener((List<Article>) result);
			} else {
				listenerArticle.onGetErrorListArticleListener(ctx
						.getString(R.string.ErrorNullListArticleMessageResult));
			}
		}

	}

}
