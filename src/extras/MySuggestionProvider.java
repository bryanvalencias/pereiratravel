package extras;

import android.content.SearchRecentSuggestionsProvider;

public class MySuggestionProvider extends SearchRecentSuggestionsProvider {

	public final static String AUTHORITY = "controls.MySuggestionProvider";
	public final static int MODE = DATABASE_MODE_QUERIES;

	public MySuggestionProvider() {
		this.setupSuggestions(AUTHORITY, MODE);

	}

}
