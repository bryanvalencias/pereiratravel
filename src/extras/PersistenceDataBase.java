package extras;

import static android.provider.BaseColumns._ID;

import java.util.ArrayList;
import java.util.List;

import model.Article;
import model.Category;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class PersistenceDataBase extends SQLiteOpenHelper {

	private static String DB_NAME = "db_persistence_data_pereiratravel";

	// TABLES
	private static String TABLE_CATEGORIES = "table_categories";
	private static String TABLE_ARTICLE = "table_article";
	private static String TABLE_KEYWORDS = "table_keywords";

	// COLUMMS TABLE_CATEGORIES
	private static String COLUMN_CATEGORY_IDCATEGORY = "columm_category_id_category";
	private static String COLUMN_CATEGORY_IDROOT = "columm_category_id_root";
	private static String COLUMN_CATEGORY_NAME = "columm_category_name";
	private static String COLUMN_CATEGORY_IMAGEPATH = "columm_category_imagepath";

	// COLUMMS TABLE_ARTICLES
	private static String COLUMN_ARTICLE_DATE = "columm_article_date";
	private static String COLUMN_ARTICLE_TITLE = "columm_article_title";
	private static String COLUMN_ARTICLE_TEXT = "columm_article_text";
	private static String COLUMN_ARTICLE_IMAGE = "columm_article_image";

	// COLUMNS TABLE_KEYWORDS
	private static String COLUMN_KEYWORD_NAME = "column_keyword_name";

	// QUERY CREATE TABLE CATEGORIES
	private String queryCreateTableCategories = "CREATE TABLE "
			+ TABLE_CATEGORIES + " (" + _ID
			+ " INTEGER PRIMARY KEY AUTOINCREMENT, "
			+ COLUMN_CATEGORY_IDCATEGORY + " INTEGER, "
			+ COLUMN_CATEGORY_IDROOT + " INTEGER, " + COLUMN_CATEGORY_NAME
			+ " TEXT, " + COLUMN_CATEGORY_IMAGEPATH + " TEXT)";

	private String queryCreateTableArticle = "CREATE TABLE " + TABLE_ARTICLE
			+ " (" + _ID + " TEXT, " + COLUMN_ARTICLE_DATE + " TEXT, "
			+ COLUMN_ARTICLE_TITLE + " TEXT, " + COLUMN_ARTICLE_TEXT
			+ " TEXT, " + COLUMN_ARTICLE_IMAGE + " TEXT)";

	private String queryCreateTableKeyWords = "CREATE TABLE " + TABLE_KEYWORDS
			+ " (" + _ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
			+ COLUMN_KEYWORD_NAME + " TEXT)";

	private String queryDrop = "DROP TABLE IF EXIST ";

	public PersistenceDataBase(Context context) {
		super(context, DB_NAME, null, 1);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(this.queryCreateTableCategories);
		db.execSQL(this.queryCreateTableArticle);
		db.execSQL(this.queryCreateTableKeyWords);

	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL(queryDrop + TABLE_CATEGORIES);
		db.execSQL(queryDrop + TABLE_ARTICLE);
		db.execSQL(queryDrop + TABLE_KEYWORDS);
	}

	/* get columns Categories */
	private String[] getColumnCategory() {
		String[] columns = new String[5];
		columns[0] = _ID;
		columns[1] = PersistenceDataBase.COLUMN_CATEGORY_IDCATEGORY;
		columns[2] = PersistenceDataBase.COLUMN_CATEGORY_IDROOT;
		columns[3] = PersistenceDataBase.COLUMN_CATEGORY_NAME;
		columns[4] = PersistenceDataBase.COLUMN_CATEGORY_IMAGEPATH;
		return columns;
	}

	public void registerCategoriesToDb(List<Category> listCategories) {
		for (Category category : listCategories) {
			if (!this.isCheckIdStored(TABLE_CATEGORIES,
					this.getColumnCategory(), category.getIdCategory())) {
				ContentValues values = new ContentValues();
				values.put(COLUMN_CATEGORY_IDCATEGORY, category.getIdCategory());
				values.put(COLUMN_CATEGORY_IDROOT, category.getIdRoot());
				values.put(COLUMN_CATEGORY_NAME, category.getName());
				values.put(COLUMN_CATEGORY_IMAGEPATH, category.getImagesPath());
				this.getWritableDatabase().insert(TABLE_CATEGORIES, null,
						values);
				Log.i("CATEGORY_REGISTER_TODB",
						String.valueOf(category.getIdCategory()) + " : "
								+ category.getName());
			}
		}
	}

	public void registerArticleToDb(List<Article> listArticle) {
		for (Article article : listArticle) {
			ContentValues values = new ContentValues();
			values.put(COLUMN_ARTICLE_DATE, article.getDate());// beta
			values.put(COLUMN_ARTICLE_TITLE, article.getTitle());// beta
			values.put(COLUMN_ARTICLE_TEXT, article.getText());
			values.put(COLUMN_ARTICLE_IMAGE, article.getImage());
			this.getWritableDatabase().insert(TABLE_ARTICLE, null, values);
			Log.i("ARTICLE_REGISTER_TODB", article.getTitle());

		}
	}

	public void registerKeyWordsToDb(List<String> listKeyWords) {
		for (String keyword : listKeyWords) {
			ContentValues values = new ContentValues();
			values.put(COLUMN_KEYWORD_NAME, keyword);
			this.getWritableDatabase().insert(TABLE_KEYWORDS, null, values);
			Log.i("KEYWORD_REGISTER_TODB", String.valueOf(keyword));
		}
	}

	/* get array all keywords from database */
	public List<String> getArrayAllkeyWords() {
		List<String> listkeyWords = new ArrayList<String>();
		Cursor cursor;
		cursor = this.getReadableDatabase().query(TABLE_KEYWORDS, null, null,
				null, null, null, null);
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			listkeyWords.add(cursor.getString(1));
			cursor.moveToNext();
		}
		cursor.close();
		return listkeyWords;
	}

	public List<Category> getArrayCategories() {
		List<Category> listCategory = new ArrayList<Category>();
		Cursor cursor;
		cursor = this.getReadableDatabase().query(TABLE_CATEGORIES, null, null,
				null, null, null, null);
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			if (cursor.getInt(2) == 0) {
				Category cat = new Category();
				cat.setIdCategory(cursor.getInt(1));
				cat.setIdRoot(cursor.getInt(2));
				cat.setName(cursor.getString(3));
				cat.setImagesPath(cursor.getString(4));
				listCategory.add(cat);
			}
			cursor.moveToNext();
		}
		cursor.close();
		return listCategory;
	}

	/* get array all categories from database */
	public List<Category> getArrayAllCategories() {
		List<Category> listCategory = new ArrayList<Category>();
		Cursor cursor;
		cursor = this.getReadableDatabase().query(TABLE_CATEGORIES, null, null,
				null, null, null, null);
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			Category cat = new Category();
			cat.setIdCategory(cursor.getInt(1));
			cat.setIdRoot(cursor.getInt(2));
			cat.setName(cursor.getString(3));
			cat.setImagesPath(cursor.getString(4));
			listCategory.add(cat);
			cursor.moveToNext();
		}
		cursor.close();
		return listCategory;
	}

	public List<Category> getArraySubCategories(int idRoot) {
		List<Category> listCategory = new ArrayList<Category>();
		Cursor cursor;
		cursor = this.getReadableDatabase().query(TABLE_CATEGORIES, null, null,
				null, null, null, null);
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			Category cat = new Category();
			if (cursor.getInt(2) == idRoot) {
				cat.setIdCategory(cursor.getInt(1));
				cat.setIdRoot(cursor.getInt(2));
				cat.setName(cursor.getString(3));
				listCategory.add(cat);
			}
			cursor.moveToNext();
		}
		cursor.close();
		return listCategory;
	}

	public List<Article> getArrayArticle() {
		List<Article> listArticle = new ArrayList<Article>();
		Cursor cursor;
		cursor = this.getReadableDatabase().query(TABLE_ARTICLE, null, null,
				null, null, null, null);
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			Article art = new Article();
			art.setDate(cursor.getString(1));
			art.setTitle(cursor.getString(2));
			art.setText(cursor.getString(3));
			art.setImage(cursor.getString(4));
			listArticle.add(art);
			cursor.moveToNext();
		}
		cursor.close();
		return listArticle;
	}

	/* counts the number of records of Categories */
	public int getCountCategories() {
		Cursor cursor;
		cursor = this.getReadableDatabase().query(TABLE_CATEGORIES, null, null,
				null, null, null, null);
		int count = cursor.getCount();
		cursor.close();
		return count;
	}

	/* counts the number of records of Article */
	public int getCountArticle() {
		Cursor cursor;
		cursor = this.getReadableDatabase().query(TABLE_ARTICLE, null, null,
				null, null, null, null);
		int count = cursor.getCount();
		cursor.close();
		return count;
	}

	/* counts the number of records of Categories */
	public int getCountKeyWords() {
		Cursor cursor;
		cursor = this.getReadableDatabase().query(TABLE_KEYWORDS, null, null,
				null, null, null, null);
		int count = cursor.getCount();
		cursor.close();
		return count;
	}

	/*
	 * check whether category id is already stored and avoid storing one
	 * category repeated
	 */
	private boolean isCheckIdStored(String table, String[] columns,
			int idCategory) {
		Cursor cursor;
		cursor = this.getReadableDatabase().query(table, columns, null, null,
				null, null, null);
		if (cursor.getCount() == 0) {
			return false;
		}
		cursor.moveToFirst();
		// tour the cursor to check whether category's ids are already stored
		while (!cursor.isAfterLast()) {
			if (cursor.getInt(1) == idCategory)
				return true;
			cursor.moveToNext();
		}
		return false;
	}

	/* Delete TABLE_CATEGORIES */
	public int deleteCategoriesToDatabase() {
		return this.getWritableDatabase().delete(TABLE_CATEGORIES, null, null);
	}

	/* Delete TABLE_SITES */
	public int deleteArticleToDatabase() {
		return this.getWritableDatabase().delete(TABLE_ARTICLE, null, null);
	}

	/* Delete TABLE_KEYWORDS */
	public int deleteKeyWordsToDatabase() {
		return this.getWritableDatabase().delete(TABLE_KEYWORDS, null, null);
	}

	/* Close connection */
	public void Close() {
		this.close();
	}

	/* Open connection */
	public void Open() {
		this.getWritableDatabase();
	}

}
