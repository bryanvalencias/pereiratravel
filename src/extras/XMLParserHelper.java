package extras;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import model.Article;
import model.Category;
import model.Site;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlSerializer;

import android.util.Log;
import android.util.Xml;

public class XMLParserHelper {
	public String countItems = null;
	private static final String NS = null;
	private static final String NAMECATEGORY = "nombreCategoria";
	private static final String NAMESITE = "nombreLugar";
	private static final String IMAGE = "URLImagenCategoria";
	private static final String IMAGEURLGALERY = "URLImagen";
	private static final String LISTIMAGEURLGALERY = "listaImagenesGaleria";
	private static final String GALERY = "galeria";
	private static final String LISTGALERY = "listaGalerias";
	private static final String IMAGES = "imagen";
	private static final String IDROOT = "identificadorCategoriaRaiz";
	private static final String IDCATEGORY = "identificadorCategoria";
	private static final String IDSITES = "identificadorLugar";
	private static final String DESCRIPTIONSITE = "descripcionCortaLugar";
	private static final String SITEURL = "URLLugar";
	private static final String KEYWORDS = "listaPalabrasClave";
	private static final String KEYWORD = "palabraClave";
	private static final String ITEMSCOUNTS = "cantidadItems";
	private static final String LATITUDE = "latitudLugar";
	private static final String LONGITUDE = "longitudLugar";
	private static final String LOGO = "URLImagenLugar";
	private static final String PHONENUMBER = "telefonoLugar";
	private static final String ADDRESS = "direccionLugar";
	private static final String EMAIL = "emailLugar";
	private static final String CATEGORY = "categoria";
	private static final String ARTICULO = "articulo";
	private static final String CATEGORIES = "listaCategoriasLugares";
	private static final String SITE = "lugar";
	private static final String SITES = "listaLugares";
	private static final String SITESNEAR = "listaLugaresCercanos";
	private static final String DATOS = "datos";
	private static final String DATOSLOGINPLATAFORMA = "datosLoginPlataforma";
	private static final String NOMBREUSUARIO = "nombreUsuario";
	private static final String CONTRASENAUSUARIO = "contrasenaUsuario";
	private static final String CLAVEUSOWEBSERVICE = "claveUsoWebservice";
	private static final String METODOREQUERIDO = "metodoRequerido";
	private static final String PARAMETROSMETODO = "parametrosMetodo";
	private static final String IDENTIFICADORCATEGORIARAIZ = "identificadorCategoriaRaiz";
	private static final String PALABRACLAVE = "palabraClave";
	private static final String IDENTIFICADORCATEGORIA = "identificadorCategoria";
	private static final String RESULTSERVICES = "resultadoConsultaServicio";
	private static final String RESULTOK = "Ok";
	public static final String IDMESSAGERESULT = "identificadorMensaje";
	public static final String TYPEMESSAGERESULT = "tipoMensaje";
	public static final String TEXTMESSAGERESULT = "textoMensaje";
	public static final String TEXTAUXMESSAGERESULT = "textoAuxiliarMensaje";
	public static final String LAT = "latitudPunto";
	public static final String LONG = "longitudPunto";
	public static final String RADIO = "radioBusqueda";
	public static final String VALORCALIFICACION = "valorCalificacion";
	public static final String IDENTIFICADORMOVIL = "identificadorMovil";
	public static final String RESULTADOOPERACION = "resultadoOperacion";
	public static final String MENSAJEOPERACION = "mensajeOperacion";
	public static final String NUEVACALIFICACIONLUGAR = "nuevaCalificacionLugar";
	public static final String IDENTIFICADORCATEGORIAARTICULO = "identificadorCategoriaArticulo";
	public static final String CANTIDADREGISTROS = "cantidadRegistros";
	public static final String FECHAPUBLICACIONARTICULO = "fechaPublicacionArticulo";
	public static final String TITULOARTICULO = "tituloArticulo";
	public static final String TEXTOARTICULO = "textoArticulo";
	public static final String TEXTOARTICULOPLANO = "textoArticuloPlano";
	public static final String URLIMAGENARTICULO = "URLImagenArticulo";
	public static final String LISTAARTICULOS = "listaArticulos";
	public static final String VALORACIONESTRELLASLUGAR = "valoracionEstrellasLugar";

	public List<Category> parseDataCategory(InputStream in)
			throws XmlPullParserException, IOException {
		try {
			XmlPullParser parser = Xml.newPullParser();
			parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
			parser.setInput(in, null);
			parser.nextTag();
			return this.parseCategory(parser);
		} finally {
			in.close();
		}
	}

	public List<Site> parseDataSite(InputStream in)
			throws XmlPullParserException, IOException {
		try {
			XmlPullParser parser = Xml.newPullParser();
			parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
			parser.setInput(in, null);
			parser.nextTag();
			return this.parseSite(parser);
		} finally {
			in.close();
		}
	}

	public String[] parseRatings(InputStream in) throws XmlPullParserException,
			IOException {
		try {
			XmlPullParser parser = Xml.newPullParser();
			parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
			parser.setInput(in, null);
			parser.nextTag();
			return parseRating(parser);
		} finally {
			in.close();
		}
	}

	public List<Article> parseArticle(InputStream in)
			throws XmlPullParserException, IOException {
		try {
			XmlPullParser parser = Xml.newPullParser();
			parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
			parser.setInput(in, null);
			parser.nextTag();
			return parseArticle(parser);
		} finally {
			in.close();
		}
	}

	public List<Site> parseDataSiteNear(InputStream in)
			throws XmlPullParserException, IOException {
		try {
			XmlPullParser parser = Xml.newPullParser();
			parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
			parser.setInput(in, null);
			parser.nextTag();
			return this.parseSiteNear(parser);
		} finally {
			in.close();
		}
	}

	public List<String> parseDataKeywords(InputStream in)
			throws XmlPullParserException, IOException {
		try {
			XmlPullParser parser = Xml.newPullParser();
			parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
			parser.setInput(in, null);
			parser.nextTag();
			return this.parseKeyWords(parser);
		} finally {
			in.close();
		}
	}

	public List<Site> parseSite(XmlPullParser parser)
			throws XmlPullParserException, IOException {
		HashMap<String, String> resultList = new HashMap<String, String>();
		List<Site> siteList = new ArrayList<Site>();
		String idMessage = null;
		String typeMessage = null;
		String textMessage = null;
		String textAuxMessage = null;
		parser.require(XmlPullParser.START_TAG, NS, DATOS);
		while (parser.next() != XmlPullParser.END_TAG) {
			if (parser.getEventType() != XmlPullParser.START_TAG) {
				continue;
			}
			String getName = parser.getName();
			if (getName.equals(RESULTSERVICES)) {
				resultList = this.getResultServices(parser);
				if (resultList != null) {
					idMessage = resultList.get(IDMESSAGERESULT);
					typeMessage = resultList.get(TYPEMESSAGERESULT);
					textMessage = resultList.get(TEXTMESSAGERESULT);
					textAuxMessage = resultList.get(TEXTAUXMESSAGERESULT);
				}
			} else if (getName.equals(METODOREQUERIDO)) {
				Log.e(METODOREQUERIDO, this.readText(parser));
			} else if (getName.equals(ITEMSCOUNTS)) {
				String text = this.readText(parser);
				Log.e(ITEMSCOUNTS, text);
				this.countItems = text;
			} else if (getName.equals(SITES)) {
				if (typeMessage.equals(RESULTOK)) {
					siteList = readSites(parser);
				} else {
					Log.e(IDMESSAGERESULT, idMessage);
					Log.e(TYPEMESSAGERESULT, typeMessage);
					Log.e(TEXTMESSAGERESULT, textMessage);
					Log.e(TEXTAUXMESSAGERESULT, textAuxMessage);
					siteList = null;
				}
			} else {
				skip(parser);
			}
		}
		return siteList;

	}

	public String[] parseRating(XmlPullParser parser)
			throws XmlPullParserException, IOException {
		HashMap<String, String> resultList = new HashMap<String, String>();
		String[] RatingsResult = new String[4];
		String idMessage = null;
		String typeMessage = null;
		String textMessage = null;
		String textAuxMessage = null;
		parser.require(XmlPullParser.START_TAG, NS, DATOS);
		while (parser.next() != XmlPullParser.END_TAG) {
			if (parser.getEventType() != XmlPullParser.START_TAG) {
				continue;
			}
			String getName = parser.getName();
			if (getName.equals(RESULTSERVICES)) {
				resultList = this.getResultServices(parser);
				if (resultList != null) {
					idMessage = resultList.get(IDMESSAGERESULT);
					typeMessage = resultList.get(TYPEMESSAGERESULT);
					textMessage = resultList.get(TEXTMESSAGERESULT);
					textAuxMessage = resultList.get(TEXTAUXMESSAGERESULT);
				}
			} else if (getName.equals(METODOREQUERIDO)) {
				Log.e(METODOREQUERIDO, this.readText(parser));
			} else if (getName.equals(ITEMSCOUNTS)) {
				String text = this.readText(parser);
				Log.e(ITEMSCOUNTS, text);
				this.countItems = text;
			} else if (getName.equals(RESULTADOOPERACION)) {
				if (typeMessage.equals(RESULTOK)) {
					RatingsResult = readRatings(parser);
				} else {
					Log.e(IDMESSAGERESULT, idMessage);
					Log.e(TYPEMESSAGERESULT, typeMessage);
					Log.e(TEXTMESSAGERESULT, textMessage);
					Log.e(TEXTAUXMESSAGERESULT, textAuxMessage);
					RatingsResult = null;
				}
			} else {
				skip(parser);
			}
		}
		return RatingsResult;

	}

	public List<Article> parseArticle(XmlPullParser parser)
			throws XmlPullParserException, IOException {
		HashMap<String, String> resultList = new HashMap<String, String>();
		List<Article> ArticleResult = new ArrayList<Article>();
		String idMessage = null;
		String typeMessage = null;
		String textMessage = null;
		String textAuxMessage = null;
		parser.require(XmlPullParser.START_TAG, NS, DATOS);
		while (parser.next() != XmlPullParser.END_TAG) {
			if (parser.getEventType() != XmlPullParser.START_TAG) {
				continue;
			}
			String getName = parser.getName();
			if (getName.equals(RESULTSERVICES)) {
				resultList = this.getResultServices(parser);
				if (resultList != null) {
					idMessage = resultList.get(IDMESSAGERESULT);
					typeMessage = resultList.get(TYPEMESSAGERESULT);
					textMessage = resultList.get(TEXTMESSAGERESULT);
					textAuxMessage = resultList.get(TEXTAUXMESSAGERESULT);
				}
			} else if (getName.equals(METODOREQUERIDO)) {
				Log.e(METODOREQUERIDO, this.readText(parser));
			} else if (getName.equals(ITEMSCOUNTS)) {
				String text = this.readText(parser);
				Log.e(ITEMSCOUNTS, text);
				this.countItems = text;
			} else if (getName.equals(LISTAARTICULOS)) {
				if (typeMessage.equals(RESULTOK)) {
					ArticleResult = this.readArticles(parser);
				} else {
					Log.e(IDMESSAGERESULT, idMessage);
					Log.e(TYPEMESSAGERESULT, typeMessage);
					Log.e(TEXTMESSAGERESULT, textMessage);
					Log.e(TEXTAUXMESSAGERESULT, textAuxMessage);
					ArticleResult = null;
				}
			} else {
				skip(parser);
			}
		}
		return ArticleResult;

	}

	public List<Site> parseSiteNear(XmlPullParser parser)
			throws XmlPullParserException, IOException {
		HashMap<String, String> resultList = new HashMap<String, String>();
		List<Site> siteList = new ArrayList<Site>();
		String idMessage = null;
		String typeMessage = null;
		String textMessage = null;
		String textAuxMessage = null;
		parser.require(XmlPullParser.START_TAG, NS, DATOS);
		while (parser.next() != XmlPullParser.END_TAG) {
			if (parser.getEventType() != XmlPullParser.START_TAG) {
				continue;
			}
			String getName = parser.getName();
			if (getName.equals(RESULTSERVICES)) {
				resultList = this.getResultServices(parser);
				if (resultList != null) {
					idMessage = resultList.get(IDMESSAGERESULT);
					typeMessage = resultList.get(TYPEMESSAGERESULT);
					textMessage = resultList.get(TEXTMESSAGERESULT);
					textAuxMessage = resultList.get(TEXTAUXMESSAGERESULT);
				}
			} else if (getName.equals(METODOREQUERIDO)) {
				Log.e(METODOREQUERIDO, this.readText(parser));
			} else if (getName.equals(ITEMSCOUNTS)) {
				String text = this.readText(parser);
				Log.e(ITEMSCOUNTS, text);
				this.countItems = text;
			} else if (getName.equals(SITESNEAR)) {
				if (typeMessage.equals(RESULTOK)) {
					siteList = readSitesNear(parser);
				} else {
					Log.e(IDMESSAGERESULT, idMessage);
					Log.e(TYPEMESSAGERESULT, typeMessage);
					Log.e(TEXTMESSAGERESULT, textMessage);
					Log.e(TEXTAUXMESSAGERESULT, textAuxMessage);
					siteList = null;
				}
			} else {
				skip(parser);
			}
		}
		return siteList;

	}

	public List<Category> parseCategory(XmlPullParser parser)
			throws XmlPullParserException, IOException {
		HashMap<String, String> resultList = new HashMap<String, String>();
		List<Category> categoryList = new ArrayList<Category>();
		String idMessage = null;
		String typeMessage = null;
		String textMessage = null;
		String textAuxMessage = null;

		parser.require(XmlPullParser.START_TAG, NS, DATOS);
		while (parser.next() != XmlPullParser.END_TAG) {
			if (parser.getEventType() != XmlPullParser.START_TAG) {
				continue;
			}
			String getName = parser.getName();
			if (getName.equals(RESULTSERVICES)) {
				resultList = this.getResultServices(parser);
				if (resultList != null) {
					idMessage = resultList.get(IDMESSAGERESULT);
					typeMessage = resultList.get(TYPEMESSAGERESULT);
					textMessage = resultList.get(TEXTMESSAGERESULT);
					textAuxMessage = resultList.get(TEXTAUXMESSAGERESULT);
				}
			} else if (getName.equals(METODOREQUERIDO)) {
				Log.e(METODOREQUERIDO, this.readText(parser));
			} else if (getName.equals(CATEGORIES)) {
				if (typeMessage.equals(RESULTOK)) {
					categoryList = readCategories(parser);
				} else {
					Log.e(IDMESSAGERESULT, idMessage);
					Log.e(TYPEMESSAGERESULT, typeMessage);
					Log.e(TEXTMESSAGERESULT, textMessage);
					Log.e(TEXTAUXMESSAGERESULT, textAuxMessage);
					categoryList = null;
				}
			}
		}
		return categoryList;

	}

	private List<String> parseKeyWords(XmlPullParser parser)
			throws XmlPullParserException, IOException {
		HashMap<String, String> resultList = new HashMap<String, String>();
		List<String> keyWordsList = new ArrayList<String>();
		String idMessage = null;
		String typeMessage = null;
		String textMessage = null;
		String textAuxMessage = null;
		parser.require(XmlPullParser.START_TAG, NS, DATOS);
		while (parser.next() != XmlPullParser.END_TAG) {
			if (parser.getEventType() != XmlPullParser.START_TAG) {
				continue;
			}
			String getName = parser.getName();
			if (getName.equals(RESULTSERVICES)) {
				resultList = this.getResultServices(parser);
				if (resultList != null) {
					idMessage = resultList.get(IDMESSAGERESULT);
					typeMessage = resultList.get(TYPEMESSAGERESULT);
					textMessage = resultList.get(TEXTMESSAGERESULT);
					textAuxMessage = resultList.get(TEXTAUXMESSAGERESULT);
				}
			} else if (getName.equals(METODOREQUERIDO)) {
				Log.e(METODOREQUERIDO, this.readText(parser));
			} else if (getName.equals(KEYWORDS)) {
				if (typeMessage.equals(RESULTOK)) {
					keyWordsList = this.readKeyWords(parser);
				} else {
					Log.e(IDMESSAGERESULT, idMessage);
					Log.e(TYPEMESSAGERESULT, typeMessage);
					Log.e(TEXTMESSAGERESULT, textMessage);
					Log.e(TEXTAUXMESSAGERESULT, textAuxMessage);
					keyWordsList = null;
				}
			}
		}
		return keyWordsList;
	}

	private List<Site> readSites(XmlPullParser parser)
			throws XmlPullParserException, IOException {

		List<Site> sitesList = new ArrayList<Site>();
		parser.require(XmlPullParser.START_TAG, NS, SITES);
		while (parser.next() != XmlPullParser.END_TAG) {
			if (parser.getEventType() != XmlPullParser.START_TAG) {
				continue;
			}
			String getName = parser.getName();
			// Starts by looking for the Category tag
			if (getName.equals(SITE)) {
				sitesList.add(readSite(parser));
			} else {
				skip(parser);
			}
		}
		return sitesList;
	}

	private String[] readRatings(XmlPullParser parser)
			throws XmlPullParserException, IOException {

		String[] RatingsResult = new String[3];
		parser.require(XmlPullParser.START_TAG, NS, RESULTADOOPERACION);
		while (parser.next() != XmlPullParser.END_TAG) {
			if (parser.getEventType() != XmlPullParser.START_TAG) {
				continue;
			}
			String getName = parser.getName();
			// Starts by looking for the Category tag
			if (getName.equals(RESULTADOOPERACION)) {
				RatingsResult[0] = this.readName(parser, RESULTADOOPERACION);
			} else if (getName.equals(MENSAJEOPERACION)) {
				RatingsResult[1] = this.readName(parser, MENSAJEOPERACION);
			} else if (getName.equals(NUEVACALIFICACIONLUGAR)) {
				RatingsResult[2] = this
						.readName(parser, NUEVACALIFICACIONLUGAR);
			} else {
				skip(parser);
			}
		}
		return RatingsResult;
	}

	private List<Category> readCategories(XmlPullParser parser)
			throws XmlPullParserException, IOException {
		List<Category> Categories = new ArrayList<Category>();

		parser.require(XmlPullParser.START_TAG, NS, CATEGORIES);
		while (parser.next() != XmlPullParser.END_TAG) {
			if (parser.getEventType() != XmlPullParser.START_TAG) {
				continue;
			}
			String getName = parser.getName();
			// Starts by looking for the Category tag
			if (getName.equals(CATEGORY)) {
				Categories.add(readCategory(parser));
			} else {
				skip(parser);
			}
		}
		return Categories;
	}

	private List<Article> readArticles(XmlPullParser parser)
			throws XmlPullParserException, IOException {
		List<Article> Articles = new ArrayList<Article>();

		parser.require(XmlPullParser.START_TAG, NS, LISTAARTICULOS);
		while (parser.next() != XmlPullParser.END_TAG) {
			if (parser.getEventType() != XmlPullParser.START_TAG) {
				continue;
			}
			String getName = parser.getName();
			// Starts by looking for the Category tag
			if (getName.equals(ARTICULO)) {
				Articles.add(readArticle(parser));
			} else {
				skip(parser);
			}
		}
		return Articles;
	}

	private List<String> readKeyWords(XmlPullParser parser)
			throws XmlPullParserException, IOException {
		List<String> keyWordsList = new ArrayList<String>();
		parser.require(XmlPullParser.START_TAG, NS, KEYWORDS);
		while (parser.next() != XmlPullParser.END_TAG) {
			if (parser.getEventType() != XmlPullParser.START_TAG) {
				continue;
			}
			String getName = parser.getName();
			if (getName.equals(XMLParserHelper.KEYWORD)) {
				keyWordsList.add(this.readText(parser));
			} else {
				this.skip(parser);
			}
		}
		return keyWordsList;

	}

	private Category readCategory(XmlPullParser parser)
			throws XmlPullParserException, IOException {
		parser.require(XmlPullParser.START_TAG, NS, CATEGORY);
		String idCategory = null;
		String idRoot = null;
		String name = null;
		String imagesPath = null;
		Category category;
		while (parser.next() != XmlPullParser.END_TAG) {
			if (parser.getEventType() != XmlPullParser.START_TAG) {
				continue;
			}
			String getName = parser.getName();
			if (getName.equals(XMLParserHelper.IDCATEGORY)) {
				idCategory = this.readId(parser);
			} else if (getName.equals(XMLParserHelper.IDROOT)) {
				idRoot = this.readIdRoot(parser);
			} else if (getName.equals(XMLParserHelper.NAMECATEGORY)) {
				name = this.readName(parser);
			} else if (getName.equals(XMLParserHelper.IMAGE)) {
				imagesPath = this.readImage(parser);
			} else {
				this.skip(parser);
			}
		}
		category = new Category();
		category.setIdCategory(Integer.parseInt(idCategory));
		category.setIdRoot(Integer.parseInt(idRoot));
		category.setName(name);
		category.setImagesPath(imagesPath);
		return category;

	}

	private Article readArticle(XmlPullParser parser)
			throws XmlPullParserException, IOException {
		parser.require(XmlPullParser.START_TAG, NS, ARTICULO);
		String date = null;
		String title = null;
		String text = null;
		String imagesPath = null;
		Article article;
		while (parser.next() != XmlPullParser.END_TAG) {
			if (parser.getEventType() != XmlPullParser.START_TAG) {
				continue;
			}
			String getName = parser.getName();
			if (getName.equals(XMLParserHelper.FECHAPUBLICACIONARTICULO)) {
				date = this.readName(parser, FECHAPUBLICACIONARTICULO);
			} else if (getName.equals(XMLParserHelper.TITULOARTICULO)) {
				title = this.readName(parser, TITULOARTICULO);
			} else if (getName.equals(XMLParserHelper.TEXTOARTICULOPLANO)) {
				text = this.readName(parser, TEXTOARTICULOPLANO);
			} else if (getName.equals(XMLParserHelper.URLIMAGENARTICULO)) {
				imagesPath = this.readName(parser, URLIMAGENARTICULO);
			} else {
				this.skip(parser);
			}
		}
		article = new Article();
		article.setDate(date);
		article.setTitle(title);
		article.setText(text);
		article.setImage(imagesPath);
		return article;

	}

	private Site readSite(XmlPullParser parser) throws XmlPullParserException,
			IOException {
		parser.require(XmlPullParser.START_TAG, NS, SITE);
		String idSite = null;
		String idCategory = null;
		String logo = null;
		String fullRatings = null;
		String name = null;
		String address = null;
		String phoneNomber = null;
		String siteURL = null;
		String email = null;
		String latitude = null;
		String longitude = null;
		String description = null;
		ArrayList<String> listImages = null;
		Site site;
		while (parser.next() != XmlPullParser.END_TAG) {
			if (parser.getEventType() != XmlPullParser.START_TAG) {
				continue;
			}
			String getName = parser.getName();
			if (getName.equals(XMLParserHelper.IDSITES)) {
				idSite = this.readId(parser, IDSITES);
			} else if (getName.equals(XMLParserHelper.NAMESITE)) {
				name = this.readName(parser, NAMESITE);
			} else if (getName.equals(XMLParserHelper.IDCATEGORY)) {
				idCategory = this.readIdCategory(parser);
			} else if (getName.equals(XMLParserHelper.DESCRIPTIONSITE)) {
				description = this.readName(parser, DESCRIPTIONSITE);
			} else if (getName.equals(XMLParserHelper.LOGO)) {
				logo = this.readImage(parser, LOGO);
			} else if (getName.equals(XMLParserHelper.VALORACIONESTRELLASLUGAR)) {
				fullRatings = this.readName(parser, VALORACIONESTRELLASLUGAR);

			} else if (getName.equals(XMLParserHelper.PHONENUMBER)) {
				phoneNomber = this.readName(parser, PHONENUMBER);
			} else if (getName.equals(XMLParserHelper.ADDRESS)) {
				address = this.readName(parser, ADDRESS);
			} else if (getName.equals(XMLParserHelper.SITEURL)) {
				siteURL = this.readName(parser, SITEURL);
			} else if (getName.equals(XMLParserHelper.EMAIL)) {
				email = this.readName(parser, EMAIL);
			} else if (getName.equals(XMLParserHelper.LATITUDE)) {
				latitude = this.readName(parser, LATITUDE);
			} else if (getName.equals(XMLParserHelper.LONGITUDE)) {
				longitude = this.readName(parser, LONGITUDE);
			} else if (getName.equals(XMLParserHelper.LISTGALERY)) {
				listImages = (ArrayList<String>) this
						.readListGaleryImages(parser);

			} else {
				this.skip(parser);
			}
		}
		site = new Site();
		site.setId(Integer.parseInt(idSite));
		site.setIdCategory(Integer.parseInt(idCategory));
		site.setLogo(logo);
		site.setFullRatings(fullRatings);
		site.setName(name);
		site.setAddress(address);
		site.setPhoneNumbre(phoneNomber);
		site.setURL(siteURL);
		site.setEmail(email);
		site.setLatitude(latitude);
		site.setLongitude(longitude);
		site.setDescription(description);
		site.setImagePath(listImages);

		return site;

	}

	private List<Site> readSitesNear(XmlPullParser parser)
			throws XmlPullParserException, IOException {

		List<Site> sitesList = new ArrayList<Site>();
		parser.require(XmlPullParser.START_TAG, NS, SITESNEAR);
		while (parser.next() != XmlPullParser.END_TAG) {
			if (parser.getEventType() != XmlPullParser.START_TAG) {
				continue;
			}
			String getName = parser.getName();
			// Starts by looking for the Category tag
			if (getName.equals(SITE)) {
				sitesList.add(readSiteNear(parser));
			} else {
				skip(parser);
			}
		}
		return sitesList;
	}

	private Site readSiteNear(XmlPullParser parser)
			throws XmlPullParserException, IOException {
		parser.require(XmlPullParser.START_TAG, NS, SITE);
		String idSite = null;
		String logo = null;
		String name = null;
		String latitude = null;
		String longitude = null;
		Site site;
		while (parser.next() != XmlPullParser.END_TAG) {
			if (parser.getEventType() != XmlPullParser.START_TAG) {
				continue;
			}
			String getName = parser.getName();
			if (getName.equals(XMLParserHelper.IDSITES)) {
				idSite = this.readId(parser, IDSITES);
			} else if (getName.equals(XMLParserHelper.NAMESITE)) {
				name = this.readName(parser, NAMESITE);
			} else if (getName.equals(XMLParserHelper.LOGO)) {
				logo = this.readImage(parser, LOGO);
			} else if (getName.equals(XMLParserHelper.LATITUDE)) {
				latitude = this.readName(parser, LATITUDE);
			} else if (getName.equals(XMLParserHelper.LONGITUDE)) {
				longitude = this.readName(parser, LONGITUDE);
			} else {
				this.skip(parser);
			}
		}
		site = new Site();
		site.setId(Integer.parseInt(idSite));
		site.setLogo(logo);
		site.setName(name);
		site.setLatitude(latitude);
		site.setLongitude(longitude);
		return site;

	}

	private List<String> readListGaleryImages(XmlPullParser parser)
			throws XmlPullParserException, IOException {
		parser.require(XmlPullParser.START_TAG, NS, LISTGALERY);
		List<String> listUrlImage = new ArrayList<String>();
		while (parser.next() != XmlPullParser.END_TAG) {
			if (parser.getEventType() != XmlPullParser.START_TAG) {
				continue;
			}
			String getName = parser.getName();
			if (getName.equals(XMLParserHelper.GALERY)) {
				for (String string : this.readGaleryImages(parser)) {
					listUrlImage.add(string);

				}
			} else {
				this.skip(parser);
			}
		}
		return listUrlImage;
	}

	private List<String> readGaleryImages(XmlPullParser parser)
			throws XmlPullParserException, IOException {
		parser.require(XmlPullParser.START_TAG, NS, GALERY);
		List<String> listUrlImage = new ArrayList<String>();
		while (parser.next() != XmlPullParser.END_TAG) {
			if (parser.getEventType() != XmlPullParser.START_TAG) {
				continue;
			}
			String getName = parser.getName();
			if (getName.equals(XMLParserHelper.LISTIMAGEURLGALERY)) {
				for (String string : this.readListIamges(parser)) {
					listUrlImage.add(string);
				}
			} else {
				this.skip(parser);
			}
		}
		return listUrlImage;
	}

	private List<String> readListIamges(XmlPullParser parser)
			throws XmlPullParserException, IOException {
		parser.require(XmlPullParser.START_TAG, NS, LISTIMAGEURLGALERY);
		List<String> listUrlImage = new ArrayList<String>();
		while (parser.next() != XmlPullParser.END_TAG) {
			if (parser.getEventType() != XmlPullParser.START_TAG) {
				continue;
			}
			String getName = parser.getName();
			if (getName.equals(XMLParserHelper.IMAGES)) {
				listUrlImage.add(this.readImages(parser));
			} else {
				this.skip(parser);
			}
		}
		return listUrlImage;

	}

	private String readImages(XmlPullParser parser)
			throws XmlPullParserException, IOException {
		parser.require(XmlPullParser.START_TAG, NS, IMAGES);
		String urlImage = null;
		while (parser.next() != XmlPullParser.END_TAG) {
			if (parser.getEventType() != XmlPullParser.START_TAG) {
				continue;
			}
			String getName = parser.getName();
			if (getName.equals(XMLParserHelper.IMAGEURLGALERY)) {
				urlImage = this.readImage(parser, IMAGEURLGALERY);
			} else {
				this.skip(parser);
			}
		}
		return urlImage;
	}

	/* Processes Id tags in the Site. */
	private String readId(XmlPullParser parser) throws IOException,
			XmlPullParserException {
		parser.require(XmlPullParser.START_TAG, NS, IDCATEGORY);
		String id = readText(parser);
		parser.require(XmlPullParser.END_TAG, NS, IDCATEGORY);
		return id;
	}

	/* Processes Id tags in the Site. */
	private String readId(XmlPullParser parser, String getId)
			throws IOException, XmlPullParserException {
		parser.require(XmlPullParser.START_TAG, NS, getId);
		String id = readText(parser);
		parser.require(XmlPullParser.END_TAG, NS, getId);
		return id;
	}

	/* Processes Id tags in the Category. */
	private String readIdCategory(XmlPullParser parser) throws IOException,
			XmlPullParserException {
		parser.require(XmlPullParser.START_TAG, NS, IDCATEGORY);
		String id = readText(parser);
		parser.require(XmlPullParser.END_TAG, NS, IDCATEGORY);
		return id;
	}

	/* Processes IdRoot tags in the Category. */
	private String readIdRoot(XmlPullParser parser) throws IOException,
			XmlPullParserException {
		parser.require(XmlPullParser.START_TAG, NS, IDROOT);
		String id_root = readText(parser);
		parser.require(XmlPullParser.END_TAG, NS, IDROOT);
		return id_root;
	}

	/* Processes Name tags in the Category. */
	private String readName(XmlPullParser parser) throws IOException,
			XmlPullParserException {
		parser.require(XmlPullParser.START_TAG, NS, NAMECATEGORY);
		String name = readText(parser);
		parser.require(XmlPullParser.END_TAG, NS, NAMECATEGORY);
		return name;
	}

	/* Processes Name tags in the Category. */
	private String readName(XmlPullParser parser, String name)
			throws IOException, XmlPullParserException {
		parser.require(XmlPullParser.START_TAG, NS, name);
		String named = readText(parser);
		parser.require(XmlPullParser.END_TAG, NS, name);
		return named;
	}

	/* Processes Image tags in the Category. */
	private String readImage(XmlPullParser parser) throws IOException,
			XmlPullParserException {
		parser.require(XmlPullParser.START_TAG, NS, IMAGE);
		String image = readText(parser);
		parser.require(XmlPullParser.END_TAG, NS, IMAGE);
		return image;

	}

	private String readImage(XmlPullParser parser, String name)
			throws IOException, XmlPullParserException {
		parser.require(XmlPullParser.START_TAG, NS, name);
		String image = readText(parser);
		parser.require(XmlPullParser.END_TAG, NS, name);
		return image;

	}

	// For the tags name, and image extracts their text values.
	private String readText(XmlPullParser parser) throws IOException,
			XmlPullParserException {
		String result = "";
		if (parser.next() == XmlPullParser.TEXT) {
			result = parser.getText();
			parser.nextTag();
		}
		return result;
	}

	/* get TextAuxMessage of text Aux message result */
	private String readResultTextAuxMessageState(XmlPullParser parser)
			throws XmlPullParserException, IOException {
		parser.require(XmlPullParser.START_TAG, NS, TEXTAUXMESSAGERESULT);
		String textAux = readText(parser);
		parser.require(XmlPullParser.END_TAG, NS, TEXTAUXMESSAGERESULT);
		return textAux;
	}

	/* get TextMessage of text message result */
	private String readResultTextMessageState(XmlPullParser parser)
			throws XmlPullParserException, IOException {
		parser.require(XmlPullParser.START_TAG, NS, TEXTMESSAGERESULT);
		String text = readText(parser);
		parser.require(XmlPullParser.END_TAG, NS, TEXTMESSAGERESULT);
		return text;
	}

	/* get idType of type message result */
	private String readResultTypeMessageState(XmlPullParser parser)
			throws XmlPullParserException, IOException {
		parser.require(XmlPullParser.START_TAG, NS, TYPEMESSAGERESULT);
		String type = readText(parser);
		parser.require(XmlPullParser.END_TAG, NS, TYPEMESSAGERESULT);
		return type;
	}

	/* get idMessage of id message result */
	private String readResultIdMessageState(XmlPullParser parser)
			throws XmlPullParserException, IOException {
		parser.require(XmlPullParser.START_TAG, NS, IDMESSAGERESULT);
		String idMessage = readText(parser);
		parser.require(XmlPullParser.END_TAG, NS, IDMESSAGERESULT);
		return idMessage;
	}

	/*
	 * write xml request to get list category request: login data
	 * (name,password,keyWs) request: request method (query list category)
	 */
	public String buildXmlGetListCategory(String nameUser, String pass,
			String keyWs, String requestMethod, String idRootCategory)
			throws IllegalArgumentException, IllegalStateException, IOException {
		XmlSerializer serializer = Xml.newSerializer();
		StringWriter writer = new StringWriter();
		String messages = "";
		serializer.setOutput(writer);
		serializer.startDocument("ISO-8859-1", null);
		serializer.startTag(messages, DATOS);
		serializer.startTag(messages, DATOSLOGINPLATAFORMA);
		serializer.startTag(messages, NOMBREUSUARIO);
		serializer.text(nameUser);
		serializer.endTag(messages, NOMBREUSUARIO);
		serializer.startTag(messages, CONTRASENAUSUARIO);
		serializer.text(pass);
		serializer.endTag(messages, CONTRASENAUSUARIO);
		serializer.startTag(messages, CLAVEUSOWEBSERVICE);
		serializer.text(keyWs);
		serializer.endTag(messages, CLAVEUSOWEBSERVICE);
		serializer.endTag(messages, DATOSLOGINPLATAFORMA);
		serializer.startTag(messages, METODOREQUERIDO);
		serializer.text(requestMethod);
		serializer.endTag(messages, METODOREQUERIDO);
		serializer.startTag(messages, PARAMETROSMETODO);
		if (idRootCategory.equals("-1")) {
			serializer.startTag(messages, IDENTIFICADORCATEGORIARAIZ);
			serializer.text(idRootCategory);
			serializer.endTag(messages, IDENTIFICADORCATEGORIARAIZ);
		}
		serializer.endTag(messages, PARAMETROSMETODO);
		serializer.endTag(messages, DATOS);
		serializer.endDocument();
		Log.e("XML", writer.toString());
		return writer.toString();

	}

	/*
	 * write xml request to get list Sites request: login data
	 * (name,password,keyWs) request: request method (query list Sites)
	 */
	public String buildXmlGetListSites(String nameUser, String pass,
			String keyWs, String requestMethod, String idCategory,
			String keyword, String idSite) throws IllegalArgumentException,
			IllegalStateException, IOException {
		XmlSerializer serializer = Xml.newSerializer();
		StringWriter writer = new StringWriter();
		String messages = "";
		serializer.setOutput(writer);
		serializer.startDocument("ISO-8859-1", null);
		serializer.startTag(messages, DATOS);
		serializer.startTag(messages, DATOSLOGINPLATAFORMA);
		serializer.startTag(messages, NOMBREUSUARIO);
		serializer.text(nameUser);
		serializer.endTag(messages, NOMBREUSUARIO);
		serializer.startTag(messages, CONTRASENAUSUARIO);
		serializer.text(pass);
		serializer.endTag(messages, CONTRASENAUSUARIO);
		serializer.startTag(messages, CLAVEUSOWEBSERVICE);
		serializer.text(keyWs);
		serializer.endTag(messages, CLAVEUSOWEBSERVICE);
		serializer.endTag(messages, DATOSLOGINPLATAFORMA);
		serializer.startTag(messages, METODOREQUERIDO);
		serializer.text(requestMethod);
		serializer.endTag(messages, METODOREQUERIDO);
		serializer.startTag(messages, PARAMETROSMETODO);
		if (keyword != null) {
			serializer.startTag(messages, PALABRACLAVE);
			serializer.text(keyword);
			serializer.endTag(messages, PALABRACLAVE);
		}
		if (idCategory != null) {
			serializer.startTag(messages, IDENTIFICADORCATEGORIA);
			serializer.text(idCategory);
			serializer.endTag(messages, IDENTIFICADORCATEGORIA);
		}
		if (idSite != null) {
			serializer.startTag(messages, IDSITES);
			serializer.text(idSite);
			serializer.endTag(messages, IDSITES);
		}
		serializer.endTag(messages, PARAMETROSMETODO);
		serializer.endTag(messages, DATOS);
		serializer.endDocument();
		Log.e("XML", writer.toString());
		return writer.toString();

	}

	/*
	 * write xml request to get list Sites request: login data
	 * (name,password,keyWs) request: request method (query list Sites)
	 */
	public String buildXmlGetkeywords(String nameUser, String pass,
			String keyWs, String requestMethod)
			throws IllegalArgumentException, IllegalStateException, IOException {
		XmlSerializer serializer = Xml.newSerializer();
		StringWriter writer = new StringWriter();
		String messages = "";
		serializer.setOutput(writer);
		serializer.startDocument("ISO-8859-1", null);
		serializer.startTag(messages, DATOS);
		serializer.startTag(messages, DATOSLOGINPLATAFORMA);
		serializer.startTag(messages, NOMBREUSUARIO);
		serializer.text(nameUser);
		serializer.endTag(messages, NOMBREUSUARIO);
		serializer.startTag(messages, CONTRASENAUSUARIO);
		serializer.text(pass);
		serializer.endTag(messages, CONTRASENAUSUARIO);
		serializer.startTag(messages, CLAVEUSOWEBSERVICE);
		serializer.text(keyWs);
		serializer.endTag(messages, CLAVEUSOWEBSERVICE);
		serializer.endTag(messages, DATOSLOGINPLATAFORMA);
		serializer.startTag(messages, METODOREQUERIDO);
		serializer.text(requestMethod);
		serializer.endTag(messages, METODOREQUERIDO);
		serializer.endTag(messages, DATOS);
		serializer.endDocument();
		Log.e("XML", writer.toString());
		return writer.toString();

	}

	public String buildXmlNearby(String nameUser, String pass, String keyWs,
			String requestMethod, String Lat, String Long, String Radio,
			String idCategory) throws IllegalArgumentException,
			IllegalStateException, IOException {
		XmlSerializer serializer = Xml.newSerializer();
		StringWriter writer = new StringWriter();
		String messages = "";
		serializer.setOutput(writer);
		serializer.startDocument("ISO-8859-1", null);
		serializer.startTag(messages, DATOS);
		serializer.startTag(messages, DATOSLOGINPLATAFORMA);
		serializer.startTag(messages, NOMBREUSUARIO);
		serializer.text(nameUser);
		serializer.endTag(messages, NOMBREUSUARIO);
		serializer.startTag(messages, CONTRASENAUSUARIO);
		serializer.text(pass);
		serializer.endTag(messages, CONTRASENAUSUARIO);
		serializer.startTag(messages, CLAVEUSOWEBSERVICE);
		serializer.text(keyWs);
		serializer.endTag(messages, CLAVEUSOWEBSERVICE);
		serializer.endTag(messages, DATOSLOGINPLATAFORMA);
		serializer.startTag(messages, METODOREQUERIDO);
		serializer.text(requestMethod);
		serializer.endTag(messages, METODOREQUERIDO);
		serializer.startTag(messages, PARAMETROSMETODO);
		serializer.startTag(messages, LAT);
		serializer.text(Lat);
		serializer.endTag(messages, LAT);
		serializer.startTag(messages, LONG);
		serializer.text(Long);
		serializer.endTag(messages, LONG);
		serializer.startTag(messages, RADIO);
		serializer.text(Radio);
		serializer.endTag(messages, RADIO);
		if (idCategory != null) {
			serializer.startTag(messages, IDCATEGORY);
			serializer.text(idCategory);
			serializer.endTag(messages, IDCATEGORY);
		}
		serializer.endTag(messages, PARAMETROSMETODO);
		serializer.endTag(messages, DATOS);
		serializer.endDocument();
		Log.e("XML", writer.toString());
		return writer.toString();

	}

	/*
	 * write xml request to setlist Ratings request: login data
	 * (name,password,keyWs) request:
	 */
	public String buildXmlsetRatings(String nameUser, String pass,
			String keyWs, String requestMethod, String idsite,
			String valueRating, String idPhone)
			throws IllegalArgumentException, IllegalStateException, IOException {
		XmlSerializer serializer = Xml.newSerializer();
		StringWriter writer = new StringWriter();
		String messages = "";
		serializer.setOutput(writer);
		serializer.startDocument("ISO-8859-1", null);
		serializer.startTag(messages, DATOS);
		serializer.startTag(messages, DATOSLOGINPLATAFORMA);
		serializer.startTag(messages, NOMBREUSUARIO);
		serializer.text(nameUser);
		serializer.endTag(messages, NOMBREUSUARIO);
		serializer.startTag(messages, CONTRASENAUSUARIO);
		serializer.text(pass);
		serializer.endTag(messages, CONTRASENAUSUARIO);
		serializer.startTag(messages, CLAVEUSOWEBSERVICE);
		serializer.text(keyWs);
		serializer.endTag(messages, CLAVEUSOWEBSERVICE);
		serializer.endTag(messages, DATOSLOGINPLATAFORMA);
		serializer.startTag(messages, METODOREQUERIDO);
		serializer.text(requestMethod);
		serializer.endTag(messages, METODOREQUERIDO);
		serializer.startTag(messages, PARAMETROSMETODO);
		serializer.startTag(messages, IDSITES);
		serializer.text(idsite);
		serializer.endTag(messages, IDSITES);
		serializer.startTag(messages, VALORCALIFICACION);
		serializer.text(valueRating);
		serializer.endTag(messages, VALORCALIFICACION);
		serializer.startTag(messages, IDENTIFICADORMOVIL);
		serializer.text(idPhone);
		serializer.endTag(messages, IDENTIFICADORMOVIL);
		serializer.endTag(messages, PARAMETROSMETODO);
		serializer.endTag(messages, DATOS);
		serializer.endDocument();
		Log.e("XML", writer.toString());
		return writer.toString();

	}

	/*
	 * write xml request to setlist Ratings request: login data
	 * (name,password,keyWs) request:
	 */
	public String buildXmlgetArticle(String nameUser, String pass,
			String keyWs, String requestMethod, String idCategoryArticle,
			String numArticle) throws IllegalArgumentException,
			IllegalStateException, IOException {
		XmlSerializer serializer = Xml.newSerializer();
		StringWriter writer = new StringWriter();
		String messages = "";
		serializer.setOutput(writer);
		serializer.startDocument("ISO-8859-1", null);
		serializer.startTag(messages, DATOS);
		serializer.startTag(messages, DATOSLOGINPLATAFORMA);
		serializer.startTag(messages, NOMBREUSUARIO);
		serializer.text(nameUser);
		serializer.endTag(messages, NOMBREUSUARIO);
		serializer.startTag(messages, CONTRASENAUSUARIO);
		serializer.text(pass);
		serializer.endTag(messages, CONTRASENAUSUARIO);
		serializer.startTag(messages, CLAVEUSOWEBSERVICE);
		serializer.text(keyWs);
		serializer.endTag(messages, CLAVEUSOWEBSERVICE);
		serializer.endTag(messages, DATOSLOGINPLATAFORMA);
		serializer.startTag(messages, METODOREQUERIDO);
		serializer.text(requestMethod);
		serializer.endTag(messages, METODOREQUERIDO);
		serializer.startTag(messages, PARAMETROSMETODO);
		serializer.startTag(messages, IDENTIFICADORCATEGORIAARTICULO);
		serializer.text(idCategoryArticle);
		serializer.endTag(messages, IDENTIFICADORCATEGORIAARTICULO);
		serializer.startTag(messages, CANTIDADREGISTROS);
		serializer.text(numArticle);
		serializer.endTag(messages, CANTIDADREGISTROS);
		serializer.endTag(messages, PARAMETROSMETODO);
		serializer.endTag(messages, DATOS);
		serializer.endDocument();
		Log.e("XML", writer.toString());
		return writer.toString();

	}

	/*
	 * Skips tags the parser isn't interested in. Uses depth to handle nested
	 * 
	 * tags. i.e., if the next tag after a START_TAG isn't a matching END_TAG,
	 * it keeps going until it finds the matching END_TAG (as indicated by the
	 * value of "depth" being 0).
	 */
	private void skip(XmlPullParser parser) throws XmlPullParserException,
			IOException {
		if (parser.getEventType() != XmlPullParser.START_TAG) {
			throw new IllegalStateException();
		}
		int depth = 1;
		while (depth != 0) {
			switch (parser.next()) {
			case XmlPullParser.END_TAG:
				depth--;
				break;
			case XmlPullParser.START_TAG:
				depth++;
				break;
			}
		}
	}

	private HashMap<String, String> getResultServices(XmlPullParser parser)
			throws XmlPullParserException, IOException {
		parser.require(XmlPullParser.START_TAG, NS, RESULTSERVICES);
		HashMap<String, String> resultList = new HashMap<String, String>();

		while (parser.next() != XmlPullParser.END_TAG) {
			if (parser.getEventType() != XmlPullParser.START_TAG) {
				continue;
			}
			String getName = parser.getName();
			if (getName.equals(XMLParserHelper.IDMESSAGERESULT)) {
				resultList.put(IDMESSAGERESULT,
						this.readResultIdMessageState(parser));
			} else if (getName.equals(XMLParserHelper.TYPEMESSAGERESULT)) {
				resultList.put(TYPEMESSAGERESULT,
						this.readResultTypeMessageState(parser));
			} else if (getName.equals(XMLParserHelper.TEXTMESSAGERESULT)) {
				resultList.put(TEXTMESSAGERESULT,
						this.readResultTextMessageState(parser));
			} else if (getName.equals(XMLParserHelper.TEXTAUXMESSAGERESULT)) {
				resultList.put(TEXTAUXMESSAGERESULT,
						this.readResultTextAuxMessageState(parser));
			} else {
				this.skip(parser);
			}
		}
		return resultList;
	}
}
