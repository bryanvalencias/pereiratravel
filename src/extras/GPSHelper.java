package extras;

import android.content.Context;
import android.location.Location;
import android.os.Bundle;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;


public class GPSHelper implements GooglePlayServicesClient.ConnectionCallbacks,
		GooglePlayServicesClient.OnConnectionFailedListener, LocationListener {

	private LocationClient lc;
	private getLocationListener locationListener;

	// Milliseconds per second
	private static final int MILLISECONDS_PER_SECOND = 1000;
	// Update frequency in seconds
	public static final int UPDATE_INTERVAL_IN_SECONDS = 40;
	// Update frequency in milliseconds
	private static final long UPDATE_INTERVAL = MILLISECONDS_PER_SECOND
			* UPDATE_INTERVAL_IN_SECONDS;
	// The fastest update frequency, in seconds
	private static final int FASTEST_INTERVAL_IN_SECONDS = 40;
	// A fast frequency ceiling in milliseconds
	private static final long FASTEST_INTERVAL = MILLISECONDS_PER_SECOND
			* FASTEST_INTERVAL_IN_SECONDS;

	// Define an object that holds accuracy and frequency parameters
	private LocationRequest mLocationRequest;

	public GPSHelper(Context context, getLocationListener locationListener) {
		lc = new LocationClient(context, this, this);
		this.locationListener = locationListener;

		// Create the LocationRequest object
		mLocationRequest = LocationRequest.create();
		// Use high accuracy
		mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
		// Set the update interval to 5 seconds
		mLocationRequest.setInterval(UPDATE_INTERVAL);
		// Set the fastest update interval to 1 second
		mLocationRequest.setFastestInterval(FASTEST_INTERVAL);

	}

	public static interface getLocationListener {
		public void onConnected(Location l);

		public void onDisconnected();

		public void onLocationChange(Location location);
	}

	@Override
	public void onConnectionFailed(ConnectionResult arg0) {

	}

	@Override
	public void onConnected(Bundle arg0) {
		Location l = lastLocation();
		if (locationListener != null) {
			locationListener.onConnected(l);
			lc.requestLocationUpdates(mLocationRequest, this);

		}
	}

	@Override
	public void onDisconnected() {
		if (locationListener != null) {
			locationListener.onDisconnected();
		}

	}

	@Override
	public void onLocationChanged(Location location) {
		// Report to the UI that the location was updated
		// String msg = "Updated Location: "
		// + Double.toString(location.getLatitude()) + ","
		// + Double.toString(location.getLongitude());
		// Toast.makeText(ctx, msg, Toast.LENGTH_SHORT).show();
		locationListener.onLocationChange(location);
	}

	public void connect() {
		lc.connect();
	}

	public void Disconnect() {
		if (lc.isConnected()) {
			lc.removeLocationUpdates(this);
			lc.disconnect();
		}
	}

	public Location lastLocation() {
		try {
			return lc.getLastLocation();
		} catch (Exception e) {
			return null;
		}
	}

}
