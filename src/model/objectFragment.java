package model;

import android.support.v4.app.Fragment;

public class objectFragment {
	private int idFragment;
	private Fragment fragment;

	public objectFragment(int idFragment, Fragment fragment) {

		this.setIdFragment(idFragment);
		this.setFragment(fragment);
	}

	public int getIdFragment() {
		return idFragment;
	}

	public void setIdFragment(int idFragment) {
		this.idFragment = idFragment;
	}

	public Fragment getFragment() {
		return fragment;
	}

	public void setFragment(Fragment fragment) {
		this.fragment = fragment;
	}

}
