package model;

public class ObjectMenuDrawer {
	private int imagePath;
	private String textObject;

	public ObjectMenuDrawer(int imagePath, String textObject) {
		super();
		this.setImagePath(imagePath);
		this.setTextObject(textObject);
	}

	public int getImagePath() {
		return imagePath;
	}

	private void setImagePath(int imagePath) {
		this.imagePath = imagePath;
	}

	public String getTextObject() {
		return textObject;
	}

	private void setTextObject(String textObject) {
		this.textObject = textObject;
	}

}
