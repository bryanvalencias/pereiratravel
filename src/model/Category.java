package model;

public class Category {
	private int idCategory;
	private int idRoot;
	private String name;
	private String imagesPath;

	public Category(int idRoot, String name, String imagePath, int subId) {
		super();
		this.setIdCategory(idRoot);
		this.setName(name);
		this.setImagesPath(imagePath);
		this.setIdRoot(subId);
	}

	public Category() {
		super();
		this.setIdCategory(-1);
		this.setName("");
		this.setImagesPath(null);
		this.setIdRoot(-1);
	}

	public int getIdCategory() {
		return idCategory;
	}

	public void setIdCategory(int id) {
		this.idCategory = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getIdRoot() {
		return idRoot;
	}

	public void setIdRoot(int subId) {
		this.idRoot = subId;
	}

	public String getImagesPath() {
		return imagesPath;
	}

	public void setImagesPath(String imagesPath) {
		this.imagesPath = imagesPath;
	}

}
