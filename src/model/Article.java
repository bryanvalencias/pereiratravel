package model;

public class Article {

	private String date;
	private String title;
	private String text;
	private String image;

	public Article() {
		super();
		this.setDate(null);
		this.setTitle(null);
		this.setText(null);
		this.setImage(null);
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

}
