package model;

import java.util.List;

import android.os.Parcel;
import android.os.Parcelable;

public class Site implements Parcelable {

	private int id;
	private int idCategory;
	private String logo;
	private String fullRatings;
	private String name;
	private String address;
	private String phoneNumber;
	private String url;
	private String email;
	private String latitude;
	private String longitude;
	private String description;
	private List<String> imagePath;

	public Site() {
		this.setLogo(null);
		this.setName(null);
		this.setAddress(null);
		this.setPhoneNumbre(null);
		this.setURL(null);
		this.setEmail(null);
		this.setImagePath(null);
		this.setLatitude(null);
		this.setLongitude(null);
		this.setDescription(null);
		this.setFullRatings(null);
	}

	private Site(Parcel in) {
		readFromParcel(in);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getIdCategory() {
		return idCategory;
	}

	public void setIdCategory(int idCategory) {
		this.idCategory = idCategory;
	}

	public String getLogo() {
		return logo;
	}

	public void setLogo(String logo) {
		this.logo = logo;
	}

	public String getFullRatings() {
		return fullRatings;
	}

	public void setFullRatings(String fullRatings) {
		this.fullRatings = fullRatings;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhoneNumbre() {
		return phoneNumber;
	}

	public void setPhoneNumbre(String phoneNumbre) {
		this.phoneNumber = phoneNumbre;
	}

	public String getURL() {
		return url;
	}

	public void setURL(String cellPhoneNumbre) {
		this.url = cellPhoneNumbre;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public List<String> getImagePath() {
		return imagePath;
	}

	public void setImagePath(List<String> imagePath) {
		this.imagePath = imagePath;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	private void readFromParcel(Parcel in) {
		this.setLogo(in.readString());
		this.setName(in.readString());
		this.setAddress(in.readString());
		this.setPhoneNumbre(in.readString());
		this.setURL(in.readString());
		this.setEmail(in.readString());
		this.setLatitude(in.readString());
		this.setLongitude(in.readString());
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(this.getLogo());
		dest.writeString(this.getName());
		dest.writeString(this.getAddress());
		dest.writeString(this.getPhoneNumbre());
		dest.writeString(this.getURL());
		dest.writeString(this.getEmail());
		dest.writeString(this.getLatitude());
		dest.writeString(this.getLongitude());

	}

	public static final Parcelable.Creator<Site> CREATOR = new Parcelable.Creator<Site>() {

		@Override
		public Site createFromParcel(Parcel source) {
			return new Site(source);
		}

		@Override
		public Site[] newArray(int size) {
			return new Site[size];
		}

	};

}
